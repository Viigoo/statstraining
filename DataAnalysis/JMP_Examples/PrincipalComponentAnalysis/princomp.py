# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import PCA as mPCA
from sklearn.decomposition import PCA as skPCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import scale
import statsmodels.formula.api as smf
import scipy.stats as ss
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
from math import sqrt
import itertools
import sys

reload(sys)
sys.setdefaultencoding('utf8')


print '\33[96mPrincipal Component Analysis!\33[0m'
print '-'*50

file_name = 'StatsFiles/princomp.csv'
data = pd.read_csv(file_name, decimal=',')
# data = data*-1

data_scaled = pd.read_csv(file_name, decimal=',')
data_scaled = pd.DataFrame(scale(data_scaled), columns=data.columns.values)
# data_scaled = data_scaled*-1

pd.set_option('display.max_rows', 20)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data
print data_scaled

print '-'*50
print '\33[96mCorrelation!\33[0m'
print '-'*50

print data_scaled.corr()
corrs = data_scaled.corr().abs()
s = corrs.unstack()
so = s.sort_values(kind="quicksort")
print so[-15:-1]
print so[-33:-13:2]

# pd.plotting.scatter_matrix(data_scaled, figsize=(14,10), diagonal='kde')
# plt.show()


print '\33[96mPCA default!\33[0m'
print '\33[96m-\33[0m'*50

features = ['x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'x11', 'x12']
pca = skPCA()
pca.fit(data_scaled[features])

print 'Explained Variance:\n', pca.explained_variance_
print 'Explained Variance Ratio:\n', pca.explained_variance_ratio_
print 'Mean:\n', pca.mean_
print 'Components:\n', pca.components_
print 'Noise Variance:\n', pca.noise_variance_


print '\33[96mPrincipal components!\33[0m'
print '-'*50

prins = pca.transform(data_scaled[features])
prins_names = []
for i in range(len(features)):
    name = 'Prin' + str(i+1)
    prins_names.append(name)

# tu moze byc potencjalnie problem?
prins = pd.DataFrame(prins, columns=prins_names)
print prins

# korelacje komponentów
print '\33[96mPrincipal components correlation!\33[0m'
print prins.corr()

corrs = prins.corr().abs()
s = corrs.unstack()
so = s.sort_values(kind="quicksort")
print so[-25:-12]
print so[-33:-13:2]

# pd.plotting.scatter_matrix(prins, figsize=(14,10), diagonal='kde')
# plt.show()



# scree plot
# --------------------------
def screeplot(pca, standardised_values):
    y = np.std(pca.transform(standardised_values), axis=0)**2
    x = np.arange(len(y)) + 1
    fig = plt.figure(figsize=(9, 7), dpi=100)
    plt.plot(x, y, "o-")
    plt.xticks(x)
    # plt.xticks(x, ["Comp." + str(i) for i in x], rotation=60)
    plt.title('Scree plot', fontsize=26, y=1.01)
    plt.xlabel('Liczba głównych składowych', fontsize=20, labelpad=8)
    plt.ylabel('Wartości własne', fontsize=20, labelpad=8)
    # plt.xticks(range(len(features)))
    # plt.xticks(range(len(features)), ['{1:} ({0:.4f})'.format(i, name) for i, name in zip(pca.explained_variance_, prins_names)],
    #            rotation=60, fontsize=14)
    plt.xticks(x, [str(i) for i in x], fontsize=14)
    plt.yticks(np.arange(0, 5, 0.5), fontsize=14)
    # plt.xlim(-0.45)
    # plt.ylim(0, 1.01)
    plt.grid(True, alpha=0.5)
    plt.tight_layout()
    plt.show()


screeplot(pca, data_scaled[features])


# eigenvalues plot
# --------------------------
print pca.explained_variance_
print pca.explained_variance_ratio_
print np.cumsum(pca.explained_variance_ratio_)
fig = plt.figure(figsize=(9, 7), dpi=100)
plt.bar(range(len(features)), pca.explained_variance_ratio_,
        label='Współczynnik wyjaśnianej wariancji')
plt.plot(range(len(features)), np.cumsum(pca.explained_variance_ratio_), '--',
         label='Kumulowany wsp. wyjaśnianej wariancji')
plt.title('Wartości własne', fontsize=26, y=1.01)
plt.xlabel('Główne składowe', fontsize=20, labelpad=8)
plt.ylabel('Wkład procentowy', fontsize=20, labelpad=8)
# plt.xticks(range(len(features)))
plt.xticks(range(len(features)), ['{0:.3f}%'.format(i*100) for i in pca.explained_variance_ratio_],
           rotation=60, fontsize=14)
plt.yticks(np.arange(0, 1.01, 0.1), fontsize=14)
plt.xlim(-0.45)
plt.ylim(0, 1.01)
plt.gca().yaxis.grid(True, alpha=0.3)
legend = plt.legend(fontsize=12, frameon=True, loc='upper left')
legend.get_frame().set_edgecolor('#555555')
legend.get_frame().set_facecolor('#dddddd')
legend.get_frame().set_linewidth(1.0)
plt.tight_layout()
plt.show()



# regression
# --------------------------------------------------------
def get_formula(role_var, parameters):
    formula_string = '{} ~ '.format(role_var)
    for el in parameters:
        formula_string += el
        formula_string += ' + '
    formula_string = formula_string[:-3]
    return formula_string


features = ['x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'x11', 'x12']
result = smf.ols(formula=get_formula('y', features), data=data_scaled).fit()
print 'rsq - all vars: {0:.6f}'.format(result.rsquared)
preds = result.predict(data_scaled[features])
print 'rmse:', sqrt(mean_squared_error(data_scaled['y'], preds))
print ''

prins['y'] = data['y']
prins['yscaled'] = data_scaled['y']
features = ['Prin1', 'Prin2', 'Prin3']
result = smf.ols(formula=get_formula('y', features), data=prins).fit()
preds = result.predict(prins[features])
print 'rsq - Prin1, Prin2, Prin3: {0:.6f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(prins['y'], preds))
print ''

features = ['Prin1', 'Prin2', 'Prin3', 'Prin4']
result = smf.ols(formula=get_formula('y', features), data=prins).fit()
preds = result.predict(prins[features])
print 'rsq - Prin1, Prin2, Prin3, Prin4: {0:.6f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(prins['y'], preds))
print ''

features = ['Prin1', 'Prin2', 'Prin3', 'Prin4', 'Prin5']
result = smf.ols(formula=get_formula('y', features), data=prins).fit()
preds = result.predict(prins[features])
print 'rsq - Prin1, Prin2, Prin3, Prin4, Prin5: {0:.6f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(prins['y'], preds))
print ''


features = ['Prin1', 'Prin2', 'Prin3', 'Prin4', 'Prin5', 'Prin6',
            'Prin7', 'Prin8', 'Prin9', 'Prin10', 'Prin11', 'Prin12']
result = smf.ols(formula=get_formula('y', features), data=prins).fit()
preds = result.predict(prins[features])
print 'rsq - all prins: {0:.6f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(prins['y'], preds))
print ''

# best possible rsquared for 3 components is 0.719345 (Prin1, Prin2, Prin4)
# --------------------------------------------------------
# permutations = list(itertools.combinations(features, 3))
# rsq = []
# for perm in permutations:
#     result = smf.ols(formula=get_formula('y', perm), data=prins).fit()
#     rsq.append([result.rsquared, perm])
#
# list.sort(rsq)
# for element in rsq[-5:]:
#     print element


# best possible rsquared for 4 components is 0.7603334534 (Prin1, Prin2, Prin3, Prin4)
# --------------------------------------------------------
# permutations = list(itertools.combinations(features, 4))
# rsq = []
# for perm in permutations:
#     result = smf.ols(formula=get_formula('y', perm), data=prins).fit()
#     rsq.append([result.rsquared, perm])
#
# list.sort(rsq)
# for element in rsq[-5:]:
#     print element


# best possible rsquared for 5 components is 0.79677508 (Prin1, Prin2, Prin3, Prin4, Prin9)
# --------------------------------------------------------
# permutations = list(itertools.combinations(features, 5))
# rsq = []
# for perm in permutations:
#     result = smf.ols(formula=get_formula('y', perm), data=prins).fit()
#     rsq.append([result.rsquared, perm])
#
# list.sort(rsq)
# for element in rsq[-8:]:
#     print element




# plot components
# --------------------------
# plt.plot(prins['Prin1'], prins['Prin2'], '.')
# plt.grid(True, alpha=0.3)
# plt.show()



# loadings plot
# --------------------------
fig = plt.figure(figsize=(9, 9), dpi=100)
loading_matrix = pca.components_.T * np.sqrt(pca.explained_variance_)
plt.scatter(loading_matrix[:, 0], loading_matrix[:, 1], alpha=0.8, label="Loadings", marker='o');
ax = plt.gca()
for i, txt in enumerate(prins_names):
    ax.annotate(txt, (loading_matrix[:, 0][i]+0.015, loading_matrix[:, 1][i]))

circle = plt.Circle((0, 0), 1, fill=False, alpha=0.4)
ax.add_artist(circle)

plt.title("Loading plot");
plt.xlabel("Loadings on PC1");
plt.ylabel("Loadings on PC2");
plt.xticks([-1, -0.5, 0, 0.5, 1])
plt.yticks([-1, -0.5, 0, 0.5, 1])
plt.xlim(-1, 1)
plt.ylim(-1, 1)
plt.grid(linestyle='--', alpha=0.5)
# plt.legend()
plt.show()

print prins.corr()