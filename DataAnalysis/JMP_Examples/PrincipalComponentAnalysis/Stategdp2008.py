# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import PCA as mPCA
from sklearn.decomposition import PCA as skPCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import scale
import statsmodels.formula.api as smf
import scipy.stats as ss
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
from math import sqrt
import itertools
import sys

reload(sys)
sys.setdefaultencoding('utf8')


print '\33[96mPrincipal Component Analysis!\33[0m'
print '-'*50

file_name = 'StatsFiles/Stategdp2008.csv'
data_raw = pd.read_csv(file_name, decimal=',')

data_scaled = pd.read_csv(file_name, decimal=',')
data_scaled.drop(['state'], axis=1, inplace=True)
# data_scaled.insert(0, 'state', data['state'])


data_scaled.drop(0, inplace=True)
data_scaled.drop(['total'], axis=1, inplace=True)
data_scaled = pd.DataFrame(scale(data_scaled), columns=data_scaled.columns.values)

pd.set_option('display.max_rows', 20)
pd.set_option('display.max_columns', 70)
pd.set_option('display.width', 1000)
print data_raw
print data_scaled

print '-'*50
print '\33[96mCorrelation!\33[0m'
print '-'*50

print data_scaled.corr()
corrs = data_scaled.corr().abs()
s = corrs.unstack()
so = s.sort_values(kind="quicksort")
# print so[-15:]
print so[-35:-11]
print so.head(15)

# pd.plotting.scatter_matrix(data_scaled, figsize=(14,10), diagonal='kde')
# plt.show()







print '\33[96mPCA default!\33[0m'
print '\33[96m-\33[0m'*50

features = ['agriculture', 'mining', 'utlities', 'construction',
            'manufacturing', 'wholesaling', 'retailing',
            'transport', 'information', 'finance', 'realestate',
            'services', 'management', 'administration', 'education',
            'healthcare', 'arts', 'food', 'other', 'government']
pca = skPCA()
pca.fit(data_scaled[features])

print 'Explained Variance:\n', pca.explained_variance_
print 'Explained Variance Ratio:\n', pca.explained_variance_ratio_
print 'Mean:\n', pca.mean_
# print 'Components:\n', pca.components_
print 'Noise Variance:\n', pca.noise_variance_


print '\33[96mPrincipal components!\33[0m'
print '-'*50

prins = pca.transform(data_scaled[features])
prins_names = []
for i in range(len(features)):
    name = 'Prin' + str(i+1)
    prins_names.append(name)

# tu moze byc potencjalnie problem?
prins = pd.DataFrame(prins, columns=prins_names)
print prins

# korelacje komponentów
print '\33[96mPrincipal components correlation!\33[0m'
print prins.corr()

corrs = prins.corr().abs()
s = corrs.unstack()
so = s.sort_values(kind="quicksort")
print so[-30:-20]
print so[:20]

# pd.plotting.scatter_matrix(prins, figsize=(14,10), diagonal='kde')
# plt.show()




# scree plot
# --------------------------
def screeplot(pca, standardised_values):
    y = np.std(pca.transform(standardised_values), axis=0)**2
    x = np.arange(len(y)) + 1
    plt.plot(x, y, "o-")
    plt.xticks(x)
    # plt.xticks(x, ["Comp." + str(i) for i in x], rotation=60)
    plt.ylabel("Eigenvalue")
    plt.grid(True, alpha=0.3)
    plt.show()


# screeplot(pca, data_scaled[features])


# plot components
# --------------------------
# fig = plt.figure(figsize=(9, 9), dpi=100)
# plt.plot(prins['Prin1'], prins['Prin2'], 'o', alpha=0.7)
# plt.xticks([-20, -15, -10, -5, 0, 5, 10, 15, 20], fontsize=14)
# plt.yticks([-20, -15, -10, -5, 0, 5, 10, 15, 20], fontsize=14)
# plt.xlabel('Prin1', fontsize=20, labelpad=8)
# plt.ylabel('Prin2', fontsize=20, labelpad=4)
# plt.title('Score plot', fontsize=26, y=1.02)
# plt.grid(True, alpha=0.4)
# plt.tight_layout()
# plt.show()




# eigenvalues plot
# --------------------------
# print pca.explained_variance_
# print pca.explained_variance_ratio_
# print np.cumsum(pca.explained_variance_ratio_)
# fig = plt.figure(figsize=(9, 6), dpi=100)
# plt.bar(range(len(features)), pca.explained_variance_ratio_, label='Współczynnik wyjaśnianej wariancji')
# plt.plot(range(len(features)), np.cumsum(pca.explained_variance_ratio_), '--',
#          label='Kumulowany wsp. wyjaśnianej wariancji')
# # plt.xticks(range(len(features)))
# plt.xticks(range(len(features)), ['{0:.3f}%'.format(i*100) for i in pca.explained_variance_ratio_],
#            rotation=60, fontsize=14)
# plt.yticks(np.arange(0, 1.01, 0.1), fontsize=14)
# plt.xlim(-0.45)
# plt.ylim(0, 1.01)
# plt.xlabel('Główne składowe', fontsize=20, labelpad=8)
# plt.ylabel('Wkład procentowy', fontsize=20, labelpad=20)
# plt.title('Wartości własne', fontsize=26, y=1.02)
# plt.gca().yaxis.grid(True, alpha=0.3)
# legend = plt.legend(fontsize=14, frameon=True, loc='right')
# legend.get_frame().set_edgecolor('#555555')
# legend.get_frame().set_facecolor('#dddddd')
# legend.get_frame().set_linewidth(1.0)
# plt.tight_layout()
# plt.show()



# loadings plot
# --------------------------
fig = plt.figure(figsize=(9, 9), dpi=100)
loading_matrix = pca.components_.T * np.sqrt(pca.explained_variance_)
plt.scatter(loading_matrix[:, 0], loading_matrix[:, 1], alpha=0.8, label="Loadings", marker='o');
ax = plt.gca()
for i, txt in enumerate(features):
    ax.annotate(txt, (loading_matrix[:, 0][i]+0.015, loading_matrix[:, 1][i]))

circle = plt.Circle((0, 0), 1, fill=False, alpha=0.4)
ax.add_artist(circle)

plt.title('Loadings plot', fontsize=26, y=1.02)
plt.xlabel('Prin1', fontsize=20, labelpad=8)
plt.ylabel('Prin2', fontsize=20, labelpad=4)
plt.xticks([-1, -0.5, 0, 0.5, 1], fontsize=14)
plt.yticks([-1, -0.5, 0, 0.5, 1], fontsize=14)
plt.xlim(-1, 1)
plt.ylim(-1, 1)
plt.grid(linestyle='--', alpha=0.5)
# plt.tight_layout()
# plt.legend()
plt.show()



# ###################### #
#  RATIOS RATIOS RATIOS  #
# ###################### #



print data_raw
data_ratios = data_raw.copy()
# data_ratios['agriculture'] = data_ratios['agriculture'].div(data_ratios['total'])
for column in features:
    data_ratios[column] = data_ratios[column].div(data_ratios['total'])
data_ratios.drop(['total'], axis=1, inplace=True)
data_ratios.drop(['state'], axis=1, inplace=True)
data_ratios.drop(0, axis=0, inplace=True)
# data_ratios['sum'] = data_ratios.sum(axis=1)
print '------> ratios/scaled'
print data_ratios
data_ratios_scaled = pd.DataFrame(scale(data_ratios), columns=data_ratios.columns.values)
print data_ratios_scaled
print data_scaled



print '\33[96mPCA default!\33[0m'
print '\33[96m-\33[0m'*50

features = ['agriculture', 'mining', 'utlities', 'construction',
            'manufacturing', 'wholesaling', 'retailing',
            'transport', 'information', 'finance', 'realestate',
            'services', 'management', 'administration', 'education',
            'healthcare', 'arts', 'food', 'other', 'government']
pca = skPCA()
pca.fit(data_ratios_scaled[features])

print 'Explained Variance:\n', pca.explained_variance_
print 'Explained Variance Ratio:\n', pca.explained_variance_ratio_
print 'Mean:\n', pca.mean_
# print 'Components:\n', pca.components_
print 'Noise Variance:\n', pca.noise_variance_


print '\33[96mPrincipal components!\33[0m'
print '-'*50

prins = pca.transform(data_ratios_scaled[features])
prins_names = []
for i in range(len(features)):
    name = 'Prin' + str(i+1)
    prins_names.append(name)

# tu moze byc potencjalnie problem?
prins = pd.DataFrame(prins, columns=prins_names)
print prins

# korelacje komponentów
print '\33[96mPrincipal components correlation!\33[0m'
print prins.corr()

corrs = prins.corr().abs()
s = corrs.unstack()
so = s.sort_values(kind="quicksort")
print so[-30:-20]
print so[:20]

# pd.plotting.scatter_matrix(prins, figsize=(14,10), diagonal='kde')
# plt.show()


# screeplot(pca, data_ratios_scaled[features])



# plot components
# --------------------------
# fig = plt.figure(figsize=(9, 9), dpi=100)
# plt.plot(prins['Prin1'], prins['Prin2'], 'o', alpha=0.7)
# plt.xticks([-10, -5, 0, 5, 10], fontsize=14)
# plt.yticks([-10, -5, 0, 5, 10], fontsize=14)
# plt.xlabel('Prin1', fontsize=20, labelpad=8)
# plt.ylabel('Prin2', fontsize=20, labelpad=4)
# plt.title('Score plot', fontsize=26, y=1.02)
# plt.grid(True, alpha=0.4)
# plt.tight_layout()
# plt.show()



# eigenvalues plot
# --------------------------
# print pca.explained_variance_
# print pca.explained_variance_ratio_
# print np.cumsum(pca.explained_variance_ratio_)
# fig = plt.figure(figsize=(9, 6), dpi=100)
# plt.bar(range(len(features)), pca.explained_variance_ratio_, label='Współczynnik wyjaśnianej wariancji')
# plt.plot(range(len(features)), np.cumsum(pca.explained_variance_ratio_), '--', label='Kumulowany wsp. wyjaśnianej wariancji')
# plt.xticks(range(len(features)), ['{0:.3f}%'.format(i*100) for i in pca.explained_variance_ratio_], rotation=60, fontsize=14)
# plt.yticks(np.arange(0, 1.01, 0.1), fontsize=14)
# plt.xlim(-0.45)
# plt.ylim(0, 1.01)
# plt.xlabel('Główne składowe', fontsize=20, labelpad=8)
# plt.ylabel('Wkład procentowy', fontsize=20, labelpad=20)
# plt.title('Wartości własne', fontsize=26, y=1.02)
# plt.gca().yaxis.grid(True, alpha=0.3)
# legend = plt.legend(fontsize=14, frameon=True, loc='right')
# legend.get_frame().set_edgecolor('#555555')
# legend.get_frame().set_facecolor('#dddddd')
# legend.get_frame().set_linewidth(1.0)
# plt.tight_layout()
# plt.show()





# loadings plot
# --------------------------
fig = plt.figure(figsize=(9, 9), dpi=100)
loading_matrix = pca.components_.T * np.sqrt(pca.explained_variance_)
plt.scatter(loading_matrix[:, 0], loading_matrix[:, 1], alpha=0.8, label="Loadings", marker='o');
ax = plt.gca()
for i, txt in enumerate(features):
    ax.annotate(txt, (loading_matrix[:, 0][i]+0.015, loading_matrix[:, 1][i]))

circle = plt.Circle((0, 0), 1, fill=False, alpha=0.4)
ax.add_artist(circle)

plt.title('Loadings plot', fontsize=26, y=1.02)
plt.xlabel('Prin1', fontsize=20, labelpad=8)
plt.ylabel('Prin2', fontsize=20, labelpad=4)
plt.xticks([-1, -0.5, 0, 0.5, 1], fontsize=14)
plt.yticks([-1, -0.5, 0, 0.5, 1], fontsize=14)
plt.xlim(-1, 1)
plt.ylim(-1, 1)
plt.grid(linestyle='--', alpha=0.5)
plt.tight_layout()
# plt.legend()
plt.show()

# ax = plt.gca()
# for i, txt in enumerate(features):
#     ax.annotate(txt, (loading_matrix[:, 0][i]+0.015, loading_matrix[:, 1][i]))
#
# circle = plt.Circle((0, 0), 1, fill=False, alpha=0.4)
# ax.add_artist(circle)
#
# plt.title('Loadings plot', fontsize=26, y=1.02)
# plt.xlabel('Prin1', fontsize=20, labelpad=8)
# plt.ylabel('Prin2', fontsize=20, labelpad=4)
# plt.xticks([-1, -0.5, 0, 0.5, 1], fontsize=14)
# plt.yticks([-1, -0.5, 0, 0.5, 1], fontsize=14)
# plt.xlim(-1, 1)
# plt.ylim(-1, 1)
# plt.grid(linestyle='--', alpha=0.5)
# # plt.tight_layout()
# # plt.legend()
# plt.show()

