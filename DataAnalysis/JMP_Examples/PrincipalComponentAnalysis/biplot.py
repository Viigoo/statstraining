# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import PCA as mPCA
from sklearn.decomposition import PCA as skPCA
from sklearn.preprocessing import StandardScaler


# def biplot(score,coeff,pcax,pcay,labels=None):
#     pca1 = pcax-1
#     pca2 = pcay-1
#     xs = score[:,pca1]
#     ys = score[:,pca2]
#     n=score.shape[1]
#     scalex = 1.0/(xs.max()- xs.min())
#     scaley = 1.0/(ys.max()- ys.min())
#     plt.scatter(xs*scalex,ys*scaley)
#     for i in range(n):
#         plt.arrow(0, 0, coeff[i,pca1], coeff[i,pca2],color='r',alpha=0.5)
#         if labels is None:
#             plt.text(coeff[i,pca1]* 1.15, coeff[i,pca2] * 1.15, "Var"+str(i+1), color='g', ha='center', va='center')
#         else:
#             plt.text(coeff[i,pca1]* 1.15, coeff[i,pca2] * 1.15, labels[i], color='g', ha='center', va='center')
#     plt.xlim(-1,1)
#     plt.ylim(-1,1)
#     plt.xlabel("PC{}".format(pcax))
#     plt.ylabel("PC{}".format(pcay))
#     plt.grid()


def rev(arr):
    temp = []
    for value in arr:
        temp.append(-value)
    return temp



file_name = 'StatsFiles/toyprincomp.csv'
data = pd.read_csv(file_name, decimal=',')
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data.head(10)

features = ['x', 'z']
xz = data.loc[:, features].values
y = data.loc[:, ['y']].values

xz = StandardScaler().fit_transform(xz)

print '\33[96mPCA default!\33[0m'

pca = skPCA()
pca.fit(xz)

print 'Explained Variance:\n', pca.explained_variance_
print 'Explained Variance Ratio:\n', pca.explained_variance_ratio_
print 'Mean:\n', pca.mean_
print 'Components:\n', pca.components_
print 'Noise Variance:\n', pca.noise_variance_

xz_pca = pca.transform(xz)

prin1 = xz_pca[:, 0]
prin1 = rev(prin1)
prin2 = xz_pca[:, 1]


# creating a biplot

pca = skPCA(n_components=2).fit(xz)
reduced_data = pca.transform(xz)
pca_samples = pca.transform(xz)
reduced_data = pd.DataFrame(reduced_data, columns = ['Dimension 1', 'Dimension 2'])


def biplot(data, reduced_data, pca):
    fig, ax = plt.subplots(figsize=(14, 8))

    # scatterplot of the reduced data
    ax.scatter(x=reduced_data.loc[:, 'Dimension 1'],
               y=reduced_data.loc[:, 'Dimension 2'], facecolors='k',
               edgecolors='r', s=70, alpha=0.8)

    feature_vectors = pca.components_.T

    # using scaling factors to make the arrows
    arrow_size, text_pos = 7.0, 8.0,

    # projections of the original features
    for i, v in enumerate(feature_vectors):
        ax.arrow(0, 0, arrow_size * v[0], arrow_size * v[1], head_width=0.2,
                 head_length=0.2, linewidth=2, color='red')
        ax.text(v[0] * text_pos, v[1] * text_pos, data.columns[i],
                color='black', ha='center', va='center', fontsize=18)

    ax.set_xlabel("Dimension 1", fontsize=14)
    ax.set_ylabel("Dimension 2", fontsize=14)
    ax.set_title("PC plane with original feature projections.", fontsize=16);
    return ax


ax = biplot(data, reduced_data, pca)
plt.show()