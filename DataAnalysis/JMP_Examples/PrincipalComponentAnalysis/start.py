# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA as skPCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import scale
import statsmodels.formula.api as smf
from sklearn.metrics import mean_squared_error
from math import sqrt
import sys

reload(sys)
sys.setdefaultencoding('utf8')


def rev(arr):
    temp = []
    for value in arr:
        temp.append(-value)
    return temp


print '\33[96mPrincipal Component Analysis!\33[0m'
print '-'*50

file_name = 'StatsFiles/toyprincomp.csv'
data = pd.read_csv(file_name, decimal=',')
# data = data*-1

data_scaled = pd.read_csv(file_name, decimal=',')
data_scaled = pd.DataFrame(scale(data_scaled), columns=data.columns.values)
# data_scaled = data_scaled*-1

pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data.head(10)
print data_scaled.head(10)


print '\33[96mCorrelation matrix!\33[0m'

print data.corr()


features = ['x', 'z']
xz = data.loc[:, features].values
y = data.loc[:, ['y']].values

xz = StandardScaler().fit_transform(xz)
# from sklearn.preprocessing import scale
# print pd.DataFrame(scale(data))

print xz
print y

print '\33[96mPCA default!\33[0m'

pca = skPCA()
pca.fit(xz)

print 'Explained Variance:\n', pca.explained_variance_
print 'Explained Variance Ratio:\n', pca.explained_variance_ratio_
print 'Mean:\n', pca.mean_
print 'Components:\n', pca.components_
print 'Noise Variance:\n', pca.noise_variance_

print '\33[96mPrincipal Components:\33[0m'
xz_pca = pca.transform(xz)

print xz_pca

prin1 = xz_pca[:, 0]
# prin1 = rev(prin1)
prin2 = xz_pca[:, 1]

print prin1
print prin2

xz_pca = pd.DataFrame(xz_pca)
xz_pca.columns = ['Prin1', 'Prin2']
print xz_pca


print '\33[96mPlots:\33[0m'

# correlation plot
# --------------------------
# mlp.rcParams['xtick.color'] = 'r'
plt.rcParams['xtick.major.size'] = 5
plt.rcParams['ytick.major.size'] = 5
sm = pd.plotting.scatter_matrix(data, figsize=(9,9), diagonal='kde', marker='.', s=120, alpha=0.8);
for ax in sm.ravel():
    ax.set_xlabel(ax.get_xlabel(), fontsize=20)
    ax.set_ylabel(ax.get_ylabel(), fontsize=20)
    print
# plt.suptitle('Macierz wykresów punktowych', fontsize=26)
plt.tight_layout()
plt.show()



# plot components
# --------------------------
# plt.plot(prin1, prin2, '.')
plt.plot(xz_pca['Prin1'], xz_pca['Prin2'], '.')
plt.grid(True, alpha=0.3)
plt.xticks([-2, -1, 0, 1, 2])
plt.yticks([-2, -1, 0, 1, 2])
plt.show()



# loadings plot
# --------------------------
# loadings = pca.components_  # should use lodaing_matrix according to stackoverflow
# plt.scatter(*loadings, alpha=0.3, label="Loadings");
#
# fig = plt.figure(figsize=(9, 9), dpi=100)
# loading_matrix = pca.components_.T * np.sqrt(pca.explained_variance_)
# plt.scatter(loading_matrix[:, 0], loading_matrix[:, 1], alpha=0.8, label="Loadings");
# ax = plt.gca()
# for i, txt in enumerate(['z', 'x']):
#     ax.annotate(txt, (loading_matrix[:, 0][i]+0.015, loading_matrix[:, 1][i]))
#
# circle = plt.Circle((0, 0), 1, fill=False, alpha=0.4)
# ax.add_artist(circle)
#
# plt.title("Loading plot");
# plt.xlabel("Loadings on PC1");
# plt.ylabel("Loadings on PC2");
# plt.xticks([-1, -0.5, 0, 0.5, 1])
# plt.yticks([-1, -0.5, 0, 0.5, 1])
# plt.xlim(-1, 1)
# plt.ylim(-1, 1)
# plt.grid(linestyle='dashed')
# # plt.legend()
# plt.show()


# scree plot
# --------------------------
# def screeplot(pca, standardised_values):
#     y = np.std(pca.transform(standardised_values), axis=0)**2
#     x = np.arange(len(y)) + 1
#     plt.plot(x, y, "o-")
#     plt.xticks(x, ["Comp."+str(i) for i in x], rotation=60)
#     plt.ylabel("Variance")
#     plt.show()
#
#
# screeplot(pca, xz)









# regression
# --------------------------------------------------------
def get_formula(role_var, parameters):
    formula_string = '{} ~ '.format(role_var)
    for el in parameters:
        formula_string += el
        formula_string += ' + '
    formula_string = formula_string[:-3]
    return formula_string


param_names = ['x', 'z']
# data['intercept'] = [1 for i in range(len(data))]
result = smf.ols(formula=get_formula('y', param_names), data=data).fit()
preds = result.predict(data[param_names])
print 'rsq: {0:.52f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(data['y'], preds))

param_names = ['x']
# data['intercept'] = [1 for i in range(len(data))]
result = smf.ols(formula=get_formula('y', param_names), data=data).fit()
preds = result.predict(data[param_names])
print 'rsq: {0:.52f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(data['y'], preds))

param_names = ['z']
# data['intercept'] = [1 for i in range(len(data))]
result = smf.ols(formula=get_formula('y', param_names), data=data).fit()
preds = result.predict(data[param_names])
print 'rsq: {0:.52f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(data['y'], preds))


param_names = ['Prin1', 'Prin2']
xz_pca['y'] = data['y']
result = smf.ols(formula=get_formula('y', param_names), data=xz_pca).fit()
preds = result.predict(xz_pca[param_names])
print 'rsq: {0:.52f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(data['y'], preds))

param_names = ['Prin1']
result = smf.ols(formula=get_formula('y', param_names), data=xz_pca).fit()
preds = result.predict(xz_pca[param_names])
print 'rsq: {0:.52f}'.format(result.rsquared)
print 'rmse:', sqrt(mean_squared_error(data['y'], preds))


print xz_pca.corr()

# ss.bartlett()



print data
print data_scaled
data['|'] = '|'
std = np.std(data['x'])
mean = np.mean(data['x'])
data['x_scaled'] = (data['x'] - mean) / std
std = np.std(data['z'])
mean = np.mean(data['z'])
data['z_scaled'] = (data['z'] - mean) / std
std = np.std(data['y'])
mean = np.mean(data['y'])
data['y_scaled'] = (data['y'] - mean) / std
print data
print data.cov()
print data.var(ddof=0).round(2)
print data.mean().round(2)

lol = pd.DataFrame()
std = np.std(data['x'])
mean = np.mean(data['x'])
lol['x'] = (data['x'] - np.mean(data['x'])) / np.std(data['x'])
print lol

print xz_pca.corr().round(4)