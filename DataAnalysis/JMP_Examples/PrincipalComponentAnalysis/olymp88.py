# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import PCA as mPCA
from sklearn.decomposition import PCA as skPCA
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import scale
import statsmodels.formula.api as smf
import scipy.stats as ss
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
from math import sqrt
import itertools
import pylab
import sys

reload(sys)
sys.setdefaultencoding('utf8')

print '\33[96mPrincipal Component Analysis!\33[0m'
print '-'*50

file_name = 'StatsFiles/olymp88sas.csv'
data = pd.read_csv(file_name, decimal=',')
# data = data*-1

data_scaled = pd.read_csv(file_name, decimal=',')
# data_scaled['highjump'] = data_scaled['highjump']*1000
# data_scaled['r100m'] = data_scaled['r100m']*1000
data_scaled = pd.DataFrame(scale(data_scaled), columns=data.columns.values)
# data_scaled = data_scaled*-1

pd.set_option('display.max_rows', 30)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data
print data_scaled
# print data.cov()
# print data_scaled.cov()

print '-'*50
print '\33[96mCorrelation!\33[0m'
print '-'*50

print data_scaled.corr()
corrs = data_scaled.corr().abs()
s = corrs.unstack()
so = s.sort_values(kind="quicksort")
# print so[-15:]
print so[-35:-11:2]
print so.head(15)

# pd.plotting.scatter_matrix(data_scaled, figsize=(14,10), diagonal='kde')
# plt.show()



print '\33[96mPCA default!\33[0m'
print '\33[96m-\33[0m'*50

features = ['r100m', 'longjump', 'shot', 'highjump', 'r400m',
            'h110m', 'discus', 'polevlt', 'javelin', 'r1500m']
pca = skPCA()
pca.fit(data_scaled[features])

print 'Explained Variance:\n', pca.explained_variance_
print 'Explained Variance Ratio:\n', pca.explained_variance_ratio_
print 'Mean:\n', pca.mean_
print 'Components:\n', pca.components_
print 'Noise Variance:\n', pca.noise_variance_


print '\33[96mPrincipal components!\33[0m'
print '-'*50

prins = pca.transform(data_scaled[features])
prins_names = []
for i in range(len(features)):
    name = 'Prin' + str(i+1)
    prins_names.append(name)

# tu moze byc potencjalnie problem?
prins = pd.DataFrame(prins, columns=prins_names)
print prins


# # korelacje komponentów
print '\33[96mPrincipal components correlation!\33[0m'
print prins.corr()
corrs = prins.corr().abs()
s = corrs.unstack()
so = s.sort_values(kind="quicksort")
print so[-15:]
print so.head(15)

# pd.plotting.scatter_matrix(prins, figsize=(14,10), diagonal='kde')
# plt.show()


# scree plot
# --------------------------
def screeplot(pca, standardised_values):
    y = np.std(pca.transform(standardised_values), axis=0)**2
    x = np.arange(len(y)) + 1
    plt.plot(x, y, "o-")
    plt.xticks(x)
    # plt.xticks(x, ["Comp." + str(i) for i in x], rotation=60)
    plt.ylabel("Eigenvalue")
    plt.grid(True, alpha=0.3)
    plt.show()


# screeplot(pca, data_scaled[features])


# eigenvalues plot
# --------------------------
print pca.explained_variance_
print pca.explained_variance_ratio_
print np.cumsum(pca.explained_variance_ratio_)
fig = plt.figure(figsize=(9, 7), dpi=100)
plt.bar(range(len(features)), pca.explained_variance_ratio_,
        label='Współczynnik wyjaśnianej wariancji')
plt.plot(range(len(features)), np.cumsum(pca.explained_variance_ratio_),
         '--', label='Kumulowany wsp. wyjaśnianej wariancji')
# plt.xticks(range(len(features)))
plt.xticks(range(len(features)), ['{1:} ({0:.2f}%)'.format(i*100, name) for i, name in zip(pca.explained_variance_ratio_, prins_names)],
           rotation=60, fontsize=14)
plt.yticks(np.arange(0, 1.01, 0.1), fontsize=14)
plt.xlim(-0.45)
plt.ylim(0, 1.05)
plt.xlabel('Główne składowe', fontsize=20, labelpad=8)
plt.ylabel('Wkład procentowy', fontsize=20, labelpad=20)
plt.title('Wartości własne', fontsize=26, y=1.02)
plt.gca().yaxis.grid(True, alpha=0.4)
legend = plt.legend(fontsize=10, frameon=True, loc='upper left')
legend.get_frame().set_edgecolor('#555555')
legend.get_frame().set_facecolor('#dddddd')
legend.get_frame().set_linewidth(1.0)
plt.tight_layout()
plt.show()



# plot components
# --------------------------
fig = plt.figure(figsize=(9, 5), dpi=100)
plt.plot(prins['Prin1'], prins['Prin2'], 'o', alpha=0.7)
plt.xticks([-10, -5, 0, 5, 10], fontsize=14)
plt.yticks([-5, 0, 5], fontsize=14)
# plt.xticks(range(len(features)), ['{1:} ({0:.2f}%)'.format(i*100, name) for i, name in zip(pca.explained_variance_ratio_, prins_names)],
#            rotation=60, fontsize=14)
# plt.yticks(np.arange(0, 1.01, 0.1), fontsize=14)
# plt.xlim(-0.45)
# plt.ylim(0, 1.05)
plt.xlabel('Prin1', fontsize=20, labelpad=8)
plt.ylabel('Prin2', fontsize=20, labelpad=4)
plt.title('Score plot', fontsize=26, y=1.02)
plt.grid(True, alpha=0.4)
# legend = plt.legend(fontsize=14, frameon=True)
# legend.get_frame().set_edgecolor('#555555')
# legend.get_frame().set_facecolor('#dddddd')
# legend.get_frame().set_linewidth(1.0)
plt.tight_layout()
plt.show()



# loadings plot
# --------------------------
fig = plt.figure(figsize=(9, 9), dpi=100)
loading_matrix = pca.components_.T * np.sqrt(pca.explained_variance_)
plt.scatter(loading_matrix[:, 0], loading_matrix[:, 1], alpha=0.7, label="Loadings", marker='o');
ax = plt.gca()
for i, txt in enumerate(features):
    ax.annotate(txt, (loading_matrix[:, 0][i]+0.015, loading_matrix[:, 1][i]), fontsize=12)

circle = plt.Circle((0, 0), 1, fill=False, alpha=0.4)
ax.add_artist(circle)

plt.title('Loadings plot', fontsize=26, y=1.02)
plt.xlabel('Prin1', fontsize=20, labelpad=8)
plt.ylabel('Prin2', fontsize=20, labelpad=8)
plt.xticks([-1, -0.5, 0, 0.5, 1], fontsize=14)
plt.yticks([-1, -0.5, 0, 0.5, 1], fontsize=14)
plt.xlim(-1, 1)
plt.ylim(-1, 1)
plt.grid(linestyle='--', alpha=0.5)
# plt.legend()
plt.show()


print prins.corr()
print loading_matrix
