# coding=utf-8
import pandas as pd
import pandas.plotting
import re
import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sci
import sklearn.feature_selection
import statsmodels.api as sm
import statsmodels.formula.api as smf
import patsy
print 'Housing Prices Example (ver.2.0)'
print '-'*50

content = []
file_name = 'StatsFiles/HousingPrices.csv'
with open(file_name, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        content.append(row)

# convert values to floats and separate values from titles
raw_titles = content.pop(0)
content_titles = []
for i in raw_titles:
    content_titles.append(i.replace(' ', '_'))

content_floats = []
for row in content:
    row_float = []
    for element in row:
        row_float.append(float(element.replace(',', '.')))
    content_floats.append(row_float)

price = []
beds = []
baths = []
square_feet = []
miles_to_resort = []
miles_to_base = []
acres = []
cars = []
years_old = []
dom = []
for row in content_floats:
    price.append(row[0])
    beds.append(row[1])
    baths.append(row[2])
    square_feet.append(row[3])
    miles_to_resort.append(row[4])
    miles_to_base.append(row[5])
    acres.append(row[6])
    cars.append(row[7])
    years_old.append(row[8])
    dom.append(row[9])

# display data array, naive
# ------------------------------------------
# print '\tTask:', file_name
# print '/', '-'*195, '\\'
# for title in content_titles:
#     print '|{:^17s}|'.format(title),
# print ''
# print '|', '-'*195, '|'
# for row in content_floats:
#     for idx, element in enumerate(row):
#         print '|{:>11.3f}      |'.format(element),
#     print ''
# print '\\', '-'*195, '/'

# display data
# ------------------------------------------
data = pandas.DataFrame(content_floats, columns=content_titles)
# data = pandas.read_csv(file_name) # reads as string
pandas.set_option('display.max_rows', 100)
pandas.set_option('display.max_columns', 100)
pandas.set_option('display.width', 500)
print data
print '-'*180

# cook
model = smf.ols(formula='Price ~ Baths', data=data)
fitted = model.fit()
print fitted.summary()
