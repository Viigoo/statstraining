# coding=utf-8
import pandas
import pandas.plotting as pdplt
import csv
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import scipy.stats
from statsmodels.stats.outliers_influence import variance_inflation_factor

print 'Housing Prices Example'
print '-'*50

# read data from .csv file and put it into content array as strings
content = []
file_name = 'StatsFiles/HousingPrices.csv'
with open(file_name, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        content.append(row)

# convert values to floats and separate values from titles
raw_titles = content.pop(0)
content_titles = []
for i in raw_titles:
    content_titles.append(i.replace(' ', '_'))

content_floats = []
for row in content:
    row_float = []
    for element in row:
        row_float.append(float(element.replace(',', '.')))
    content_floats.append(row_float)

price = []
beds = []
baths = []
square_feet = []
miles_to_resort = []
miles_to_base = []
acres = []
cars = []
years_old = []
dom = []
for row in content_floats:
    price.append(row[0])
    beds.append(row[1])
    baths.append(row[2])
    square_feet.append(row[3])
    miles_to_resort.append(row[4])
    miles_to_base.append(row[5])
    acres.append(row[6])
    cars.append(row[7])
    years_old.append(row[8])
    dom.append(row[9])

# display data array, naive
# ------------------------------------------
# print '\tTask:', file_name
# print '/', '-'*195, '\\'
# for title in content_titles:
#     print '|{:^17s}|'.format(title),
# print ''
# print '|', '-'*195, '|'
# for row in content_floats:
#     for idx, element in enumerate(row):
#         print '|{:>11.3f}      |'.format(element),
#     print ''
# print '\\', '-'*195, '/'

# display data
# ------------------------------------------
data = pandas.DataFrame(content_floats, columns=content_titles)
# data = pandas.read_csv(file_name)
pandas.set_option('display.max_rows', 100)
pandas.set_option('display.max_columns', 100)
pandas.set_option('display.width', 500)
print data

print '-'*180
# now data is stored in 2 formats: raw arrays, and pandas DataFrame;
# latter one is used because of easier access to some functions

# Exploring variables one at a time (simple analysis)
# ------------------------------------------------------------
def subplot_histbox(rows, columns, pos, data, bins, title_str):
    plt.subplot(rows, columns, pos)
    plt.title(title_str)
    plt.hist(data, bins=bins, edgecolor='black', orientation="horizontal")
    plt.ylim(bins[0], bins[-1])  # reverse order
    plt.subplot(rows, columns, pos+1)
    plt.boxplot(data, showmeans=True, flierprops=dict(alpha=0.2))
    plt.ylim(bins[0], bins[-1])
    plt.axis('off')

fig = plt.figure(figsize=(18, 9), dpi=100)
subplot_histbox(2, 10, 1, price, range(100, 701, 100), 'Price')
subplot_histbox(2, 10, 3, beds, range(0, 9), 'Beds')
subplot_histbox(2, 10, 5, baths, np.linspace(0.5, 5, 10), 'Baths')
subplot_histbox(2, 10, 7, square_feet, range(500, 4001, 500), 'Square Feet')
subplot_histbox(2, 10, 9, miles_to_resort, range(-10, 61, 10), 'Miles to Resort')
subplot_histbox(2, 10, 11, miles_to_base, range(0, 56, 5), 'Miles to Base')
subplot_histbox(2, 10, 13, acres, range(0, 46, 5), 'Acres')
subplot_histbox(2, 10, 15, cars, range(-1, 7), 'Cars')
subplot_histbox(2, 10, 17, years_old, range(0, 91, 10), 'Years Old')
subplot_histbox(2, 10, 19, dom, range(0, 451, 50), 'DoM')
plt.show()


def print_basicsummary(data, title_str=''):
    percentiles = [100, 99.5, 97.5, 90, 75, 50, 25, 10, 2.5, 0.5, 0]
    percentiles_values = []
    for p in percentiles:
        percentiles_values.append(np.percentile(data, p, interpolation='nearest'))
    print title_str
    print 'Quantiles:'
    for q_perc, q_val in zip(percentiles, percentiles_values):
        print '|', '%5.1f%% | %8.1f' % (q_perc, q_val), '|'
    print 'Summary Statistics:'
    std = np.std(data, ddof=1)
    n = len(data)
    print 'Mean        : {:>8.3f}'.format(np.mean(data))
    print 'Std Dev     : {:>8.3f}'.format(std)
    print 'Std Err Mean: {:>8.3f}'.format(std / np.sqrt(n))
    print 'N           : {:>4d}'.format(n)
    print '-'*50


print_basicsummary(price, 'Price')
print_basicsummary(beds, 'Beds')
print_basicsummary(baths, 'Baths')
print_basicsummary(square_feet, 'Square Feet')
print_basicsummary(miles_to_resort, 'Miles to Resort')
print_basicsummary(miles_to_base, 'Miles to Base')
print_basicsummary(acres, 'Acres')
print_basicsummary(cars, 'Cars')
print_basicsummary(years_old, 'Years Old')
print_basicsummary(dom, 'DoM')

# Exploring Many Variables at a Time - Correlations
# -------------------------------------------------------
plt.close('all')
print 'Correlations'
print data.corr()
df_miles = data[['Price', 'Miles_to_Resort', 'Miles_to_Base']]
print ''
print df_miles.corr()
# big linear correlation of 0.948 between Miles to Resort and Miles to Base
# indicate that those variables are likely redundant. It will be shown later
# with VIF statistics (both will have VIF factors higher than 10)

pdplt.scatter_matrix(df_miles, figsize=(8, 8), c='k')
plt.show()
print '-'*180

# Building the Model
# -------------------------------------------------------
plt.close('all')
print 'Model'


def get_formula(role_var, parameters):
    formula_string = '{} ~ '.format(role_var)
    for el in param_names:
        formula_string += el
        formula_string += ' + '
    formula_string = formula_string[:-3]
    return formula_string


print '\n\nEffect summary with BATHS'
param_names = ['Beds', 'Baths', 'Square_Feet', 'Miles_to_Resort',
               'Miles_to_Base', 'Acres', 'Cars', 'Years_Old', 'DoM']
result = smf.ols(formula=get_formula('Price', param_names), data=data).fit()
print result.summary()
# If 'P>|t|' is higher than 0.05, that means that the 5% interval value
# contains 0. For example Beds P>|t| = 0.543 and coefficient is (-22.54;42.073)
# only Baths and Acres are below 0.05 level for now.

print '\n\nEffect summary without BATHS'
param_names.remove('Baths')
result = smf.ols(formula=get_formula('Price', param_names), data=data).fit()
print result.summary()
# now, after removing Baths, Acres is no longer significant, but Square Feet is.

print '-'*180

# Multicollinearity - Variance Inflation Factor (VIF)
# -------------------------------------------------------
plt.close('all')
print 'Multicollinearity'


def print_vifs(par_names, vif, thresh=10):
    print '.'*30
    print '\tVIF',
    for param in zip(par_names, vif):
        if param[0] == 'Intercept':
            continue
        print '\n{:<17s}-{:>8.3f}'.format(param[0], param[1]),
        if param[1] > thresh:
            print '-Warning-',
    print ''


def get_vifs(cols):
    ck = np.column_stack(cols)
    vif = [variance_inflation_factor(ck, i) for i in range(ck.shape[1])]
    return vif

# adding intercept with ones, as it is not added by default
intercept = [1 for i in range(len(price))]
param_names = ['Intercept', 'Beds', 'Baths', 'Square_Feet', 'Miles_to_Resort',
               'Miles_to_Base', 'Acres', 'Cars', 'Years_Old', 'DoM']
columns = [intercept, beds, baths, square_feet, miles_to_resort,
           miles_to_base, acres, cars, years_old, dom]
vif_1 = get_vifs(columns)
print_vifs(param_names, vif_1)

# checking VIFs without Miles to Resort variable
param_names.remove('Miles_to_Resort')
del columns[4]
vif_2 = get_vifs(columns)
print_vifs(param_names, vif_2)
# VIF factors of 13.8 and 14.5 indicated that there was multicollinearity
# between those variables. After removing one of them, all of the factors are
# now below threshold (meaning that there is likely no multicollinearity)

print '-'*180
# Stepwise regression
# -------------------------------------------------------
plt.close('all')
print 'Stepwise regression'


def forward_selected(data, response):
    """Linear model designed by forward selection.

    Parameters:
    -----------
    data : pandas DataFrame with all possible predictors and response

    response: string, name of response column in data

    Returns:
    --------
    model: an "optimal" fitted statsmodels linear model
           with an intercept
           selected by forward selection
           evaluated by adjusted R-squared
    """
    remaining = set(data.columns)
    remaining.remove(response)
    selected = []
    current_score, best_new_score = 0.0, 0.0
    while remaining and current_score == best_new_score:
        scores_with_candidates = []
        for candidate in remaining:
            formula = "{} ~ {} + 1".format(response,
                                           ' + '.join(selected + [candidate]))
            score = smf.ols(formula, data).fit().rsquared_adj
            scores_with_candidates.append((score, candidate))
        scores_with_candidates.sort()
        best_new_score, best_candidate = scores_with_candidates.pop()
        if current_score < best_new_score:
            remaining.remove(best_candidate)
            selected.append(best_candidate)
            current_score = best_new_score
    formula = "{} ~ {} + 1".format(response,
                                   ' + '.join(selected))
    model = smf.ols(formula, data).fit()
    return model

model_forward = forward_selected(data, 'Price')
print model_forward.summary()

print '-'*180
# Residual by Predicted Plot
# -------------------------------------------------------
plt.close('all')
print 'Residual by Predicted Plot'
f_inter = 197.1502
f_baths = 59.2081
f_miles = -3.7985
f_acres = 5.0061
f_square = 0.0511
price_diff = []
price_predicted = []
for i in range(0, len(baths)):
    prediction = f_inter + f_baths*baths[i] + f_miles*miles_to_base[i] + \
        f_acres*acres[i] + f_square*square_feet[i]
    price_predicted.append(prediction)
    price_diff.append(price[i] - prediction)

fig = plt.figure(figsize=(18, 9), dpi=100, facecolor='#aaaaaaff')
x = range(100, 700, 10)
plt.title('Residual by Predicted Plot')
plt.xlabel('Price Predicted')
plt.ylabel('Price Residual')
plt.xlim(100, 700)
plt.grid(True, color='black', alpha=0.1)
plt.plot(price_predicted, price_diff, 'bo', alpha=0.6)
plt.plot(x, [0 for i in range(100, 700, 10)], '.', color='#00005590')
plt.show()

print '-'*180
# there can be seen a pattern in this plot - smallest residuals are in the middle
# we'll try to find in the next step what is the reason behind this

# Cook's Distance Values
# which observations have large influence on model?
# -------------------------------------------------------
plt.close('all')
print "Cook's distances"
fig, ax = plt.subplots(figsize=(18, 9), facecolor='#aaaaaaff')
plt.grid(True, color='black', alpha=0.1)
fig = sm.graphics.influence_plot(model_forward, ax=ax, criterion="cooks")
plt.title('Cook distances (bigger point - bigger cook D value)')
plt.show()
# observation no. 6 is highly influencial (40 acres farm)

print '-'*180

# Stepwise regression WITHOUT influencial observations and
# Residual by Predicted Plot again
# -------------------------------------------------------
plt.close('all')
print 'Stepwise regression without observations'

data = pandas.DataFrame(data.drop(6))
model_forward = forward_selected(data, 'Price')
print model_forward.summary()

f_inter = 175.4663
f_baths = 63.6172
f_miles = -3.0250
f_acres = 12.4629
f_square = 0.0473
baths.pop(6)
miles_to_base.pop(6)
acres.pop(6)
square_feet.pop(6)
price.pop(6)
price_diff = []
price_predicted = []
for i in range(0, len(baths)):
    prediction = f_inter + f_baths*baths[i] + f_miles*miles_to_base[i] + \
        f_acres*acres[i] + f_square*square_feet[i]
    price_predicted.append(prediction)
    price_diff.append(price[i] - prediction)

fig = plt.figure(figsize=(18, 9), dpi=100, facecolor='#aaaaaaff')
x = range(100, 700, 10)
plt.title('Residual by Predicted Plot')
plt.xlabel('Price Predicted')
plt.ylabel('Price Residual')
plt.xlim(100, 700)
plt.grid(True, color='black', alpha=0.1)
plt.plot(price_predicted, price_diff, 'bo', alpha=0.6)
plt.plot(x, [0 for i in range(100, 700, 10)], '.', color='#00005590')
plt.show()
# Now residuals appear more randomly scattered around the center line

print '-'*180
# Checking whether residuals are normally distributed
# -------------------------------------------------------
plt.close('all')
print 'Residuals histogram'
shapiro_st, shapiro_pval = scipy.stats.shapiro(price_diff)
fig = plt.figure(figsize=(18, 9), dpi=100, facecolor='#aaaaaaff')
title_str = 'Residuals histogram, Shapiro-Wilk stat: {}, p-value: {}'.\
    format(shapiro_st, shapiro_pval)
plt.title(title_str)
plt.xlabel('Residuals')
plt.ylabel('Count')
plt.hist(price_diff, bins=9, edgecolor='black')
plt.show()
print title_str

print '-'*180
