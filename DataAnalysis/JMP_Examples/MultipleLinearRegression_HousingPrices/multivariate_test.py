# coding=utf-8
import pandas
import statsmodels.api as sm
from statsmodels.formula.api import ols
import re
import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sci
import sklearn.feature_selection
# read data from .csv file and put it into content array as strings
content = []
file_name = 'StatsFiles/HousingPrices.csv'
with open(file_name, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        content.append(row)

# convert values to floats and separate values from titles
content_titles = content.pop(0)
content_floats = []
for row in content:
    row_float = []
    for element in row:
        row_float.append(float(element.replace(',', '.')))
    content_floats.append(row_float)

price = []
beds = []
baths = []
square_feet = []
miles_to_resort = []
miles_to_base = []
acres = []
cars = []
years_old = []
dom = []
for row in content_floats:
    price.append(row[0])
    beds.append(row[1])
    baths.append(row[2])
    square_feet.append(row[3])
    miles_to_resort.append(row[4])
    miles_to_base.append(row[5])
    acres.append(row[6])
    cars.append(row[7])
    years_old.append(row[8])
    dom.append(row[9])

# data = pandas.DataFrame([["A", 4, 0, 1, 27],
#                          ["B", 7, 1, 1, 29],
#                          ["C", 6, 1, 0, 23],
#                          ["D", 2, 0, 0, 20],
#                          ["etc.", 3, 0, 1, 21]],
#                          columns=["ID", "score", "male", "age20", "BMI"])
data = pandas.DataFrame([[1, 1, 1],
                         [2, 4, 8],
                         [3, 9, 27]],
                        columns=["Price", "Beds", "Baths"])

print data.corr()

# model = ols("BMI ~ score + male + age20", data=data).fit()
# print model.params
# print model.summary()