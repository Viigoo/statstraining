# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
from scipy import stats
import sys

reload(sys)
sys.setdefaultencoding('utf8')

np.random.seed(753)
data = pd.DataFrame()
# x =         [10, 10.2, 10.5, 11, 12, 12.7, 13, 13, 13.4, 13.8, 19, 20, 21, 22, 22.4]
# intercept = [1,  1,  1,  1,  1,    1, 1,  1,  1,  1,  1,  1,  1,  1,  1]
# y =         [1,  1,  1,  1,  0,    1, 1,  1,  1,  1,  0,  0,  0,  0,  0]
x1 = np.random.normal(0, 2, 150)
y1 = [0] * len(x1)
x2 = np.random.normal(10, 4, 50)
y2 = [1] * len(x2)
x3 = np.random.normal(25, 6, 20)
y3 = [0] * len(x3)
x = list(x1) + list(x2) + list(x3)
y = y1 + y2 + y3
intercept = [1] * len(x)


data['x'] = x
data['y'] = y
data['intercept'] = intercept
print data.head(5)

Y = ['y']
X = [i for i in data if i not in Y]

logit_model = sm.Logit(data[Y], data[X])
result = logit_model.fit(disp=True)
print result.summary()

inter = result.params['intercept']
coeff = result.params['x']

half_point = -inter/coeff

def get_prob(n):
    return 1./(1 + np.e**(-(inter + coeff*n)))

x_start = -30
x_end = 50
ylim_start = -1
ylim_end = 2

fig = plt.figure(figsize=(18, 9), dpi=100)
plt.scatter(x, y, color='k', alpha=0.35, marker='x', s=120, label='Obserwacje')

ax_x = np.linspace(x_start, x_end, 200)
plt.plot(ax_x, get_prob(ax_x), 'g--', alpha=0.7, label='Regresja logistyczna')

slope, intercept_lin, r_value, p_value, std_err = stats.linregress(x, y)


y_start = intercept_lin + slope*x_start
y_end = intercept_lin + slope*x_end

# plt.plot([x_start, x_end],
#          [y_start, y_end],
#          'r--',
#          alpha=0.7,
#          label='Linear regression')

print 'Logistic intercept:', inter
print 'Logistic coeff:', coeff
print 'Linear intercept:', intercept_lin
print 'Linear coeff:', slope
print 'Half point:', half_point

# plt.scatter(half_point, 0.5, color='k', alpha=1, marker='o', s=120, label='Half point')
# plt.plot([half_point, half_point],
#          [ylim_start, ylim_end],
#          'k--',
#          alpha=0.4)

plt.grid(True, color='k', alpha=0.2)
plt.xlabel('Zmienna niezależna', fontsize=22, labelpad=8)
plt.ylabel('Zmienna odpowiedzi', fontsize=22, labelpad=8)
# plt.title('Logistic regression - no linearity within independent variable', fontsize=20)
xticks = range(x_start, x_end, 5)
plt.xticks(xticks, fontsize=16)
plt.yticks(fontsize=16)
plt.ylim(ylim_start, ylim_end)
plt.xlim(x_start, x_end)
# legend = plt.legend(fontsize=20, frameon=True)
# legend.get_frame().set_edgecolor('k')
# legend.get_frame().set_facecolor('#000000')
# legend.get_frame().set_linewidth(2.0)
# legend.get_frame().set_facecolor('#bbbbbb')
legend = plt.legend(fontsize=18, frameon=True, loc='best')
legend.get_frame().set_edgecolor('#555555')
legend.get_frame().set_facecolor('#dddddd')
legend.get_frame().set_linewidth(1.0)
plt.show()
# fig.savefig('LogisticNonlinearity_753.png')
# log(P/(1-P)) = intercept + coef*x
# P = 1/(1 + e^-(int + coef*x))