# coding=utf-8
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
import pylab as pl
import numpy as np
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import confusion_matrix

def Find_Optimal_Cutoff(target, predicted):
    """ Find the optimal probability cutoff point for a classification model related to event rate
    Parameters
    ----------
    target : Matrix with dependent or target data, where rows are observations

    predicted : Matrix with predicted data, where rows are observations

    Returns
    -------
    list type, with optimal cutoff value

    """
    fpr, tpr, threshold = roc_curve(target, predicted)
    i = np.arange(len(tpr))
    roc = pd.DataFrame({'tf' : pd.Series(tpr-(1-fpr), index=i), 'threshold' : pd.Series(threshold, index=i)})
    roc_t = roc.ix[(roc.tf-0).abs().argsort()[:1]]

    return list(roc_t['threshold'])

# read the data in
df = pd.read_csv("StatsFiles/Titanic.csv")

pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
df.drop('Home_Destination', 1, inplace=True)
df.drop('Name', 1, inplace=True)
df["Age"].fillna(df["Age"].median(skipna=True), inplace=True)
df['Port'].fillna(df['Port'].value_counts().idxmax(), inplace=True)
df['Fare'].fillna(df['Fare'].median(skipna=True), inplace=True)
df['Survived']=(df['Survived']=='Yes').astype(int)

cat_vars=['Passenger_Class', 'Sex', 'Port']
for var in cat_vars:
    # cat_list='var'+'_'+var
    cat_list = pd.get_dummies(df[var], prefix=var)
    data1=df.join(cat_list)
    df=data1

print '\33[96mremove initial variables\033[0m'
titanic_vars=df.columns.values.tolist()
to_keep=[i for i in titanic_vars if i not in cat_vars]

df=df[to_keep]
df.drop('Sex_male', 1, inplace=True)  # redundant
df.drop('Passenger_Class_3', 1, inplace=True)  # redundant
df.drop('Port_S', 1, inplace=True)  # redundant
print df.head()

# rename the 'rank' column because there is also a DataFrame method called 'rank'
# df.columns = ["admit", "gre", "gpa", "prestige"]

# dummify rank
# dummy_ranks = pd.get_dummies(df['prestige'], prefix='prestige')
# create a clean data frame for the regression
# cols_to_keep = ['admit', 'gre', 'gpa']
# data = df[cols_to_keep].join(dummy_ranks.ix[:, 'prestige_2':])

# manually add the intercept
df['intercept'] = 1.0

train_cols = df.columns[1:]
# fit the model
result = sm.Logit(df['Survived'], df[train_cols]).fit()
print result.summary()

# Add prediction to dataframe
df['pred'] = result.predict(df[train_cols])
print df.head()
fpr, tpr, thresholds = roc_curve(df['Survived'], df['pred'])
roc_auc = auc(fpr, tpr)
print("Area under the ROC curve : %f" % roc_auc)

####################################
# The optimal cut off would be where tpr is high and fpr is low
# tpr - (1-fpr) is zero or near to zero is the optimal cut off point
####################################
i = np.arange(len(tpr)) # index for df
roc = pd.DataFrame({'fpr' : pd.Series(fpr, index=i),'tpr' : pd.Series(tpr, index = i), '1-fpr' : pd.Series(1-fpr, index = i), 'tf' : pd.Series(tpr - (1-fpr), index = i), 'thresholds' : pd.Series(thresholds, index = i)})
roc.ix[(roc.tf-0).abs().argsort()[:1]]

# Add prediction probability to dataframe
df['pred_proba'] = result.predict(df[train_cols])

# Find optimal probability threshold
threshold = Find_Optimal_Cutoff(df['Survived'], df['pred_proba'])
print threshold

# Find prediction to the dataframe applying threshold
df['pred'] = df['pred_proba'].map(lambda x: 1 if x > threshold[0] else 0)

thres = threshold[0]*len(roc['tpr'])
print 'roc[tpr]'
print roc['tpr']
myx = np.linspace(0, 1, len(roc['tpr']))
print myx

# Plot tpr vs 1-fpr MATPLOTLIB VERSION
# fig, ax = pl.subplots()
fig = plt.figure(figsize=(9, 9), dpi=100, edgecolor='k', frameon=True)
plt.plot(myx, roc['tpr'], lw=3)
plt.plot([0, 1], [0, 1], '--', color='black', alpha=0.3, lw=2)
# pl.plot([0, len(roc['tpr'])], [0, 1], '--', color = 'black', alpha = 0.3)
# pl.plot(roc['1-fpr'], color = 'red')
# pl.plot([thres, thres], [0, 1])

plt.xlabel('FPR', fontsize=20, labelpad=8)
plt.ylabel('TPR', fontsize=20, labelpad=8)
# plt.title('Receiver operating characteristic - Logistic Regression', size=18)
plt.grid(True, alpha=0.3)
plt.xticks([0, 0.2, 0.4, 0.6, 0.8, 1], fontsize=14)
plt.yticks(fontsize=14)
plt.xlim(0, 1)
plt.ylim(0, 1)
plt.show()

# Print confusion Matrix
print 'Print confusion Matrix'
print confusion_matrix(df['Survived'], df['pred'])
print df.head(40)

# check predictions for every 0.01 threshold
t = 0
step = 0.01
effectiveness = []
while t <= 1:
    df['pred'] = df['pred_proba'].map(lambda x: 1 if x > t else 0)
    temp_sum = confusion_matrix(df['Survived'], df['pred'])[0, 0] + confusion_matrix(df['Survived'], df['pred'])[1, 1]
    effectiveness.append(temp_sum/1309.)
    t += step

pl.close('all')
pl.plot(np.arange(0, 1, step), effectiveness)
pl.grid(True, alpha=0.3)
pl.xlim(0, 1)
pl.ylim(0, 1)
pl.show()
