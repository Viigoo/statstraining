# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
import sys

reload(sys)
sys.setdefaultencoding('utf8')

print '\33[96mTitanic Useful Commands!\33[0m'
print '-'*50

# read data from .csv file and put it into content array as strings
file_name = 'StatsFiles/Titanic.csv'
data = pd.read_csv(file_name)
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data
print 'unique destinations:', len(data['Home_Destination'].unique())
print 'unique names:', len(data['Name'].unique())
print 'duplicated names:'
duplicates = data['Name'].duplicated()
print duplicates.loc[duplicates == True]

print '-'*50
print '\33[96mShow missing values\033[0m'
print data.isnull().sum()

print '\33[96mremove some variables\033[0m'
data.drop('Home_Destination', 1, inplace=True)
data.drop('Name', 1, inplace=True)

print '\33[96mreplace Age with median, port with most often occuring and fare with median\033[0m'
print('Percent of missing "Age" records is %.2f%%' %((data['Age'].isnull().sum()/float(data.shape[0]))*100))
# data.fillna(28, inplace=True)
data["Age"].fillna(data["Age"].median(skipna=True), inplace=True)
data['Port'].fillna(data['Port'].value_counts().idxmax(), inplace=True)
data['Fare'].fillna(data['Fare'].median(skipna=True), inplace=True)
print data

print '\33[96mShow missing values after modification\033[0m'
print data.isnull().sum()

print '\n\33[96mConvert yes to 1 and no to 0\033[0m'
data['Survived']=(data['Survived']=='Yes').astype(int)
# data['Sex']=(data['Sex']=='female').astype(int)


print '\n\33[96mDisplay how many times a value occured\033[0m'
print data['Survived'].value_counts()
print data['Passenger_Class'].value_counts()


print '\n\33[96mGroup each category and calculate mean for each\033[0m'
print data.groupby('Survived').mean()
print data.groupby('Passenger_Class').mean()
# print data.groupby('Passenger_Class').median()
print data.groupby('Sex').mean()
print data.groupby('Port').mean()
# print data.groupby('Parents_and_Children').mean()
# print data.groupby('Siblings_and_Spouses').mean()

# print '\33[96mplot bar chart of contingency table\033[0m'
# pd.crosstab(data['Passenger_Class'], data['Survived']).plot(kind='bar')
# plt.show()
#
# print '\33[96mcontingency stacked\033[0m'
# table = pd.crosstab(data['Passenger_Class'], data['Survived'])
# table.div(table.sum(1).astype(float), axis=0).plot(kind='bar', stacked=True)
# plt.show()

print '\33[96mage histogram\033[0m'
# data['Age'].hist(bins=12, edgecolor='k', facecolor='green')
fig = plt.figure(figsize=(18, 9), dpi=100)
bins = range(0, int(data['Age'].max()+6), 5)
data['Age'].hist(bins=bins, edgecolor='k', facecolor='#3000a0cc', label='Age')
plt.grid(False)
# plt.grid(True, alpha=0.2, color='k')
plt.gca().yaxis.grid(True, alpha=0.3, color='gray')
plt.gca().set_axisbelow(True)
plt.xticks(bins, fontsize=15)
plt.yticks(range(0,450,20), fontsize=16)
plt.xlabel('Age', fontsize=22, labelpad=8)
plt.ylabel('Liczba osób', fontsize=22, labelpad=8)
# legend = plt.legend(fontsize=14, frameon=True)
# legend.get_frame().set_edgecolor('k')
# legend.get_frame().set_facecolor('#000000')
# legend.get_frame().set_linewidth(2.0)
# legend.get_frame().set_facecolor('#bbbbbb')
# plt.title('Histogram of age of all people', fontsize=20)
plt.show()

print '\33[96mcreate dummy variables for each categorical variable\033[0m'
print '\33[96mDecide wheter to treat passenger class as categorical or continous\033[0m'
# maybe there is not linear correlation between each class and it should not be continous?
cat_vars=['Passenger_Class', 'Sex', 'Port']
# cat_vars=['Sex', 'Port']
for var in cat_vars:
    # cat_list='var'+'_'+var
    cat_list = pd.get_dummies(data[var], prefix=var)
    data1=data.join(cat_list)
    data=data1

print data
print '\33[96mremove initial variables\033[0m'
titanic_vars=data.columns.values.tolist()
to_keep=[i for i in titanic_vars if i not in cat_vars]

data=data[to_keep]
# https://github.com/statsmodels/statsmodels/issues/3367
# You need to either drop perfectly collinear columns or use a
# penalized/regularized or constrained estimator.
# data.drop('Sex_male', 1, inplace=True)  # redundant
# data.drop('Passenger_Class_3', 1, inplace=True)  # redundant
# data.drop('Port_S', 1, inplace=True)  # redundant

# "-1" coding
data['PC1'] = data.apply(
    lambda row: int(-1) if row['Passenger_Class_3'] == 1 else int(row['Passenger_Class_1']),
    axis=1
)
data['PC2'] = data.apply(
    lambda row: int(-1) if row['Passenger_Class_3'] == 1 else int(row['Passenger_Class_3']),
    axis=1
)
data['Female'] = data.apply(
    lambda row: int(1) if row['Sex_female'] == 1 else int(-1),
    axis=1
)
data['C'] = data.apply(
    lambda row: int(-1) if row['Port_S'] == 1 else int(row['Port_C']),
    axis=1
)
data['Q'] = data.apply(
    lambda row: int(-1) if row['Port_S'] == 1 else int(row['Port_Q']),
    axis=1
)
print data.head()

data.drop('Port_C', 1, inplace=True)  # redundant
data.drop('Port_Q', 1, inplace=True)  # redundant
data.drop('Port_S', 1, inplace=True)  # redundant

data.drop('Sex_female', 1, inplace=True)  # redundant
data.drop('Sex_male', 1, inplace=True)  # redundant

data.drop('Passenger_Class_1', 1, inplace=True)  # redundant
data.drop('Passenger_Class_2', 1, inplace=True)  # redundant
data.drop('Passenger_Class_3', 1, inplace=True)  # redundant

data = data.rename(columns={
    'C': 'Port_C',
    'Q': 'Port_Q',
    'Female': 'Sex_female',
    'PC1': 'Passenger_Class_1',
    'PC2': 'Passenger_Class_2'})

print data.columns.values
print '\33[96mdata with dummies\033[0m'
print '\33[96m-\033[0m'*200
print data.head(212)
# print data.head(327)
# print data.head(604)

data['intercept'] = 1.0  # manually add intercept

data_final_vars=data.columns.values.tolist()
Y=['Survived']
X=[i for i in data if i not in Y]

print '\33[96mPrint response (Y) and predictors (X)\033[0m'
print Y
print X
print data

print '\33[96mImplementing the model\033[0m'
logit_model = sm.Logit(data[Y], data[X])
result = logit_model.fit()
print result.summary()


print '\33[96mStepwise regression attempt\033[0m'
print '\33[96mRegression attributes:\033[0m'
print X
print result.summary()
# http://www.statsmodels.org/dev/generated/statsmodels.discrete.discrete_model.LogitResults.html
# very poor documentation - it's an attribute not method
print result.pvalues
print result.pvalues.values
# print result.aic
# print result.bic
# print result.bse
print result.fittedvalues
print result.pred_table()
print result.prsquared

def precision(pred_table):
    """
    Precision given pred_table. Binary classification only. Assumes group 0
    is the True.

    Analagous to (absence of) Type I errors. Probability that a randomly
    selected document is classified correctly. I.e., no false negatives.
    """
    tp, fp, fn, tn = map(float, pred_table.flatten())
    return tp / (tp + fp)
def recall(pred_table):
    """
    Precision given pred_table. Binary classification only. Assumes group 0
    is the True.

    Analagous to (absence of) Type II errors. Out of all the ones that are
    true, how many did you predict as true. I.e., no false positives.
    """
    tp, fp, fn, tn = map(float, pred_table.flatten())
    try:
        return tp / (tp + fn)
    except ZeroDivisionError:
        return np.nan
def accuracy(pred_table):
    """
    Precision given pred_table. Binary classification only. Assumes group 0
    is the True.
    """
    tp, fp, fn, tn = map(float, pred_table.flatten())
    return (tp + tn) / (tp + tn + fp + fn)
def fscore_measure(pred_table, b=1):
    """
    For b, 1 = equal importance. 2 = recall is twice important. .5 recall is
    half as important, etc.
    """
    r = recall(pred_table)
    p = precision(pred_table)
    try:
        return (1 + b**2) * r*p/(b**2 * p + r)
    except ZeroDivisionError:
        return np.nan

# wyszlo około 80%, ale 61.8% ludzi zginelo, wiec jakbysmy dali wszedzie 1 to i tak byloby nienajgorzej.
# dlatego trzeba by to sprobowac jakos poprawic
print 'precision', precision(result.pred_table())
print 'recall', recall(result.pred_table())
print 'accuracy', accuracy(result.pred_table())
print 'fscore', fscore_measure(result.pred_table())


def top_down_regression(predictors, response, alpha=0.05):
    """Logistic model designed by forward selection.

    Parameters:
    -----------
    predictor : pandas DataFrame with all possible predictors

    response: pandas DataFrame with response

    Returns:
    --------
    model: an "optimal" fitted statsmodels linear model
           with an intercept
           selected by forward selection
           evaluated by adjusted R-squared
    """
    print '\33[95mTop down regression function started.\033[0m'
    winners = []
    p = list(predictors)
    r = list(response)
    print p
    while len(p) > 1:
        temp_model = sm.Logit(data[r], data[p])
        temp_result = temp_model.fit()
        print temp_result.pvalues
        best_pv = temp_result.pvalues.min()
        best_id = temp_result.pvalues.idxmin()
        print 'Most significant predictor:', best_id, 'Best p-value:', best_pv
        if best_pv < alpha:
            winners.append(best_id)
            p.remove(best_id)
        else:
            break
    print winners
    print '\33[95mTop down regression function finished.\033[0m'
    return winners

# topdown_X = top_down_regression(X, Y)
# topdown_model = sm.Logit(data[Y], data[topdown_X])
# topdown_result = topdown_model.fit()
# print topdown_result.summary()

def forward_selected(predictors, response, alpha=0.05, locked=[], data=[]):
    """Logistic model designed by forward selection.

    Parameters:
    -----------
    predictor : pandas DataFrame with all possible predictors

    response: pandas DataFrame with response

    Returns:
    --------
    model: an "optimal" fitted statsmodels linear model
           with an intercept
           selected by forward selection
           evaluated by Pseudo R-squared
    """
    print '\33[95mForward selected regression function started.\033[0m'
    if isinstance(data, pd.DataFrame):
        print 'OK\n', data
    else:
        print 'NO DATAFRAME?'

    winners = list(locked)
    scores_with_candidates = []
    remaining = list(predictors)
    r = list(response)
    iteration = 0
    current_score = 0

    for lock in locked:
        if lock in remaining:
            # warnings.warn('\nLocked variables should be removed from candidate list!'
            #               '\nRemoving locked from candidates.')
            remaining.remove(lock)
    print 'Response:\n', r
    print 'Auto include:\n', winners
    print 'Potential candidates for including to the model:\n', remaining

    while remaining:
        for candidate in remaining:
            temp_model = sm.Logit(data[r], data[winners + [candidate]])  # join lists
            temp_result = temp_model.fit(disp=False)
            score = temp_result.prsquared
            pval = pd.DataFrame(temp_result.pvalues).at[candidate, 0]
            # print temp_result.pvalues
            # print 'pval: ', pd.DataFrame(temp_result.pvalues).at[candidate, 0]
            # print candidate, score
            # print temp_result.summary()
            scores_with_candidates.append((score, candidate, pval))
        scores_with_candidates.sort()
        best_new_score, best_candidate, best_candidate_pvalue = scores_with_candidates.pop()
        print 'Best candidate =', best_candidate
        print 'Best score =', best_new_score
        print 'Candidate pvalue =', best_candidate_pvalue
        if best_new_score > current_score and best_candidate_pvalue <= alpha:
            winners.append(best_candidate)
            remaining.remove(best_candidate)
            current_score = best_new_score
        else:
            break

    print 'Winners:\n', winners
    print '\33[95mForward selected regression function finished.\033[0m'
    return winners

forwards_X = forward_selected(X, Y, data=data)
forwards_model = sm.Logit(data[Y], data[forwards_X])
forwards_result = forwards_model.fit()
print forwards_result.summary()
print '\33[93mTesting the model.'

# print forwards_result.pvalues
# print forwards_result.pvalues.values
# print forwards_result.aic
# print forwards_result.bic
# print forwards_result.bse
testing = pd.DataFrame()
testing['Real'] = data['Survived']
testing['Fitted_Forward'] = forwards_result.fittedvalues
testing['Fitted_Simple'] = result.fittedvalues

def f(row):
    if row['Real'] == 1 and row['Fitted_Forward'] > 0:
        val = 1
    elif row['Real'] == 0 and row['Fitted_Forward'] < 0:
        val = 1
    else:
        val = 0
    return val


testing['Is_Correct'] = testing.apply(f, axis=1)
# data['Manual_fit_value'] = data.apply(g, axis=1)

print data
print testing
print np.mean(testing['Is_Correct'])

print 'precision', precision(forwards_result.pred_table())
print 'recall', recall(forwards_result.pred_table())
print 'accuracy', accuracy(forwards_result.pred_table())
print 'fscore', fscore_measure(forwards_result.pred_table())
print '*'*20
print 'precision', precision(result.pred_table())
print 'recall', recall(result.pred_table())
print 'accuracy', accuracy(result.pred_table())
print 'fscore', fscore_measure(result.pred_table())


print '\nDATA DIVIDED INTO TRAIN AND TEST\n'
from sklearn.model_selection import train_test_split
train, test, Y_train, Y_test = train_test_split(data[X], data[Y], train_size=0.7, random_state=42)
# train, test, Y_train, Y_test = train_test_split(data[X], data[Y], train_size=0.05)

train['Survived'] = Y_train
test['Survived'] = Y_test


# forward selection na treningowych
print '\n\33[93mTEST forward selection\n'
forwards_train_X_names = forward_selected(X, Y, data=train, locked=['intercept'])
forwards_model_train = sm.Logit(train[Y], train[forwards_train_X_names])
forwards_result_train = forwards_model_train.fit()
print '\n\33[92mTEST forward selection\n'
print forwards_result_train.summary()
# print np.mean(forwards_result_train.predict(test[forwards_train_X_names]))
print forwards_result_train.predict(test[forwards_train_X_names])


# simple na treningowych
print '\n\33[92mTEST simple selection\n'
logit_model_train = sm.Logit(train[Y], train[X])
simple_result_train = logit_model_train.fit()
print simple_result_train.summary()
print np.mean(simple_result_train.predict(test[X]))


print 'TRAIN'
testing2 = pd.DataFrame()
testing2['Real'] = train['Survived']
testing2['Fitted_Forward'] = forwards_result_train.fittedvalues
testing2['Fitted_Simple'] = simple_result_train.fittedvalues
print testing2

print 'TEST'
testing3 = pd.DataFrame()
testing3['Real'] = test['Survived']
testing3['Fitted_Forward'] = forwards_result_train.predict(test[forwards_train_X_names])
testing3['Fitted_Simple'] = simple_result_train.predict(test[X])
print testing3





print '\033[0m'
# Feature Selection
print '\33[96mFeature Selection\033[0m'
model = LogisticRegression()
rfe = RFE(model, 6)  # how many columns to include in model
rfe = rfe.fit(data[X], data[Y])
print rfe.support_
print rfe.ranking_
selected_features = list(data[X].columns[rfe.support_])
print 'Selected features: %s' % selected_features
print 'Implementing the model of RFE'
logit_model = sm.Logit(data[Y], data[selected_features])
result = logit_model.fit()
print result.summary()
print 'Implementing the model of RFE - sklearn'
clf = LogisticRegression()
clf.fit(data[selected_features], data[Y])
print clf.score(data[selected_features], data[Y])  # jak często trafiamy
# wyszlo około 78.7%, ale 61.8% ludzi zginelo, wiec jakbysmy dali wszedzie 1 to i tak byloby nienajgorzej.
# dlatego trzeba by to sprobowac jakos poprawic moze?
print pd.DataFrame(zip(data[selected_features].columns, np.transpose(clf.coef_)))

print '\33[96mStepwise regression attempt (sklearn)\033[0m'
# sklearn nie udostępnia p-wartości
clf = LogisticRegression()
clf.fit(data[X], data[Y])
print clf.score(data[X], data[Y])  # jak często trafiamy
# wyszlo około 78.7%, ale 61.8% ludzi zginelo, wiec jakbysmy dali wszedzie 1 to i tak byloby nienajgorzej.
# dlatego trzeba by to sprobowac jakos poprawic moze?
print pd.DataFrame(zip(data[X].columns, np.transpose(clf.coef_)))

print '\33[96mData split into test and training\033[0m'
from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(data[X], data[Y], train_size=0.7, random_state=42)
print X_train
print Y_train
print X_test
print Y_test

clf1 = LogisticRegression()
clf1.fit(X_train, Y_train)
probs = clf.predict_proba(X_test)
predicted = clf1.predict(X_test)
# print probs
# print predicted
prob = probs[:,1]
prob_df = pd.DataFrame(prob)
prob_df['predict']=np.where(prob_df[0] >= 0.50,1,0)
print prob_df.head(10)
print metrics.accuracy_score(Y_test, predicted)

print '\33[96mCross validation\033[0m'
# Cross validation
# holdout and k-fold  # http://scikit-learn.org/stable/modules/cross_validation.html
# from sklearn.cross_validation import cross_val_score  # deprecated
# from sklearn.model_selection import cross_val_score
# from sklearn.model_selection import KFold
# scores = cross_val_score(linear_model.LogisticRegression(), data[X], data[Y], scoring='accuracy', cv=8)
# print scores
# print scores.mean()

print '\33[96mROC curve\033[0m'
plt.close('all')

