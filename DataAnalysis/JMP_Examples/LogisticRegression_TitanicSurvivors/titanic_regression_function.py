# coding=utf-8
import warnings
import pandas as pd
import pandas.plotting as pdplt
import csv
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import scipy.stats
from statsmodels.stats.outliers_influence import variance_inflation_factor
from collections import Counter
from statsmodels.graphics.mosaicplot import mosaic
import seaborn as sns
from sklearn import datasets
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn import linear_model
from sklearn import metrics

intercept = -1.2015
sex_female = 2.6112 #x1
class_1 = 2.02      #x2
class_2 = 0.968     #x3
age = -0.0358       #x4
siblings = -0.3046  #x5
port_c = 0.6089     #x6


def get_probability(x1=0., x2=0., x3=0., x4=0., x5=0., x6=0.):
    return 1. / (1. + np.exp(-(intercept +
                             sex_female*x1 +
                             class_1*x2 +
                             class_2*x3 +
                             age*x4 +
                             siblings*x5 +
                             port_c*x6)))


print get_probability(x4=50)
