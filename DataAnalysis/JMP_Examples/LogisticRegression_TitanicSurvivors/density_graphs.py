# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import sys

reload(sys)
sys.setdefaultencoding('utf8')

file_name = 'StatsFiles/Titanic.csv'
data = pd.read_csv(file_name)
pd.set_option('display.max_rows', 6)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data
data["Age"].fillna(data["Age"].median(skipna=True), inplace=True)
# data['Age'].fillna(100, inplace=True)

# data['Age'].hist(normed=True)
# data['Age'].plot(kind='kde')
# plt.show()

print data[['Age', 'Survived']]
age_survived = data['Age'].loc[data['Survived'] == 'Yes']
age_died = data['Age'].loc[data['Survived'] == 'No']

# fig, (ax, ax1) = plt.subplots(1, 2, sharex='all')
fig = plt.figure(figsize=(18, 9), dpi=100)
ax = plt.axes()
plt.gca().yaxis.grid(True, alpha=0.3, color='gray')
plt.gca().set_axisbelow(True)

xs = np.linspace(0, data['Age'].max()+5, 1000)

density = gaussian_kde(age_survived)
density.covariance_factor = lambda : .2
density._compute_covariance()
plt.plot(xs, density(xs), color='g', linewidth=4, label='Odsetek osób, które przeżyły')
ax.fill_between(xs, density(xs), 0, where=density(xs) >= 0, facecolor='#00660077', interpolate=True)

density = gaussian_kde(age_died)
density.covariance_factor = lambda : .2
density._compute_covariance()
plt.plot(xs, density(xs), color='r', alpha=0.5, linewidth=4, label='Odsetek osób, które zginęły')
ax.fill_between(xs, density(xs), 0, where=density(xs) >= 0, facecolor='#66000068', interpolate=True)
# density = gaussian_kde(data['Age'])
# density.covariance_factor = lambda : .2
# density._compute_covariance()
# plt.plot(xs, density(xs))

bins = range(0, int(data['Age'].max()+6), 5)
plt.hist(data['Age'], normed=True, color='#00000022', edgecolor='k', bins=bins, label='Histogram wieku wszystkich pasażerów')

# plt.grid(True, alpha=0.1, color='k')
plt.xticks(bins, fontsize=16)
plt.yticks(np.arange(0, 0.08, 0.005), fontsize=16)
plt.xlabel('Age', fontsize=22, labelpad=8)
plt.ylabel('Odsetek pasażerów', fontsize=22, labelpad=8)

legend = plt.legend(fontsize=18, frameon=True, loc='best')
legend.get_frame().set_edgecolor('#555555')
legend.get_frame().set_facecolor('#dddddd')
legend.get_frame().set_linewidth(1.0)
# plt.title('Kernel density estimator of age of people who died and survived', fontsize=20)
plt.xlim(0, 85)
plt.show()
