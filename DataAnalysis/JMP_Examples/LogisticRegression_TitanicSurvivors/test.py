# coding=utf-8
# coding=utf-8
import pandas as pd
import pandas.plotting as pdplt
import csv
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import scipy.stats
from statsmodels.stats.outliers_influence import variance_inflation_factor
from collections import Counter
from statsmodels.graphics.mosaicplot import mosaic
import seaborn as sns
from sklearn import datasets
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn import linear_model
from sklearn import metrics
print '\33[95mForward selected regression function started.\033[0m'

content = []
file_name = 'StatsFiles/Titanic.csv'
data = pd.read_csv(file_name)
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data
print 'unique destinations:', len(data['Home_Destination'].unique())

winners = []
scores_with_candidates = []
X = ['Survived', 'Sex']
print data[X + ['Port']]

print data.at[0, 'Name']

