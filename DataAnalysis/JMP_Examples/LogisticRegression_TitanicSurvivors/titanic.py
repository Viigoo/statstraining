# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import Counter
from statsmodels.graphics.mosaicplot import mosaic

print 'Titanic Survivors Example'
print '-'*50

# read data from .csv file and put it into pandas DataFrame
file_name = 'StatsFiles/Titanic.csv'
data = pd.read_csv(file_name)
pd.set_option('display.max_rows', 6)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data


print '-'*50
# Distribution of Survived
# ------------------------------------------------------------
categories_count = Counter(data['Survived'])
print 'categories count: ', categories_count
df = pd.DataFrame.from_dict(categories_count, orient='index')
df.columns = ['Survived']  # Rename column name (was '0' by default)
df.plot(kind='bar', color='#100050ee', fontsize=16)
plt.grid(True, color='k', alpha=0.2)
plt.yticks(range(0, 1000, 100))
legend = plt.legend(fontsize=14, frameon=True)
legend.get_frame().set_edgecolor('k')
legend.get_frame().set_facecolor('#000000')
legend.get_frame().set_linewidth(2.0)
legend.get_frame().set_facecolor('#bbbbbb')
# plt.xlabel(fontsize=18)
# plt.ylabel('Response', fontsize=18)
plt.title('Distribution of Survived', fontsize=20)
# plt.show()
plt.close('all')

# Create frequencies table - add 'Prob' column and 'Total' aggregate row.
df['Prob'] = df.apply(lambda row: row/df['Survived'].sum(), axis=1)
df.loc['Total'] = df.sum()
print df

print data['Survived'].value_counts()  # display how many times a value occured

print '-'*50
# Mosaic plot - Survived versus Passenger Class and Sex
# kategoryczna zmienna vs kategoryczna
# ------------------------------------------------------------
# mosaic(data, ['Passenger_Class', 'Survived'], axes_label=True)
# plt.title('Mosaic Plot - Sur')
# plt.xlabel('Passenger Class')
# plt.ylabel('Survived')
# plt.show()

contingency1 = pd.crosstab(data['Passenger_Class'], data['Survived'])
contingency2 = pd.crosstab(data['Sex'], data['Survived'])
contingency3 = pd.crosstab(data['Siblings_and_Spouses'], data['Survived'])
contingency4 = pd.crosstab(data['Parents_and_Children'], data['Survived'])
contingency5 = pd.crosstab(data['Fare'], data['Survived'])
contingency6 = pd.crosstab(data['Port'], data['Survived'])
print contingency1
print contingency1.loc[1][0]
print contingency2
# pd.crosstab(data['Sex'], data['Survived']).plot(kind='bar')
# pd.crosstab(data['Passenger_Class'], data['Survived']).plot(kind='bar')
plt.show()

print contingency1.at[1, 'Yes']


fig = plt.figure(figsize=(12, 6), dpi=100)
# font = {'family' : 'normal',
#         # 'weight' : 'bold',
#         'size'   : 20}
# plt.rc('font', **font)
sub1 = plt.subplot(121)
ax_a = sub1.axes
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)

mosaic(data, ['Passenger_Class', 'Survived'],
       axes_label=True, ax=ax_a, labelizer=lambda k:contingency1.at[int(k[0]), str(k[1])])
plt.title('Survived by Passenger_Class', fontsize=18, y=1.01)

sub2 = plt.subplot(122)
ax_b = sub2.axes
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
mosaic(data, ['Sex', 'Survived'],
       axes_label=True, ax=ax_b, labelizer=lambda k:contingency2.at[str(k[0]), str(k[1])])
plt.title('Survived by Sex', fontsize=18, y=1.01)
plt.show()
plt.close('all')

# mosaic(data, ['Siblings_and_Spouses', 'Survived'],
#        axes_label=True, labelizer=lambda k:contingency3.at[int(k[0]), str(k[1])],
#        title='Survived by Siblings')
# mosaic(data, ['Parents_and_Children', 'Survived'],
#        axes_label=True, labelizer=lambda k:contingency4.at[int(k[0]), str(k[1])],
#        title='Survived by Children')
# mosaic(data, ['Fare', 'Survived'],
#        axes_label=True, labelizer=lambda k:contingency5.at[float(k[0]), str(k[1])],
#        title='Survived by Fare')
# mosaic(data, ['Port', 'Survived'],
#        axes_label=True, labelizer=lambda k:contingency6.at[str(k[0]), str(k[1])],
#        title='Survived by Port')
# plt.show()

# Logistic fit of Survived by Age and Parents
# kategoryczna zmienna vs. ciągła
# ------------------------------------------------------------
# plt.scatter(data['Age'], data['Parents_and_Children'])
# plt.show()
# data['Survived']=(data['Survived']=='Yes').astype(int)  # convert Yes to 1 and No to 0

























