# coding=utf-8
# coding=utf-8
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.tree import DecisionTreeClassifier
from scipy import interp
import pandas as pd
import csv
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import scipy.stats
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.tree import export_graphviz
from os import system
import pydot
import pydotplus
import collections

print 'Titanic decision tree example'
print '-'*50

# read data from .csv file and put it into content array as strings
file_name = 'StatsFiles/Titanic.csv'
data = pd.read_csv(file_name)
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data

print 'Loading completed!'
print '-'*50
print '-'*50

# ------------------------------------------------------------
data.drop('Home_Destination', 1, inplace=True)
data.drop('Name', 1, inplace=True)

data["Age"].fillna(data["Age"].median(skipna=True), inplace=True)
data['Port'].fillna(data['Port'].value_counts().idxmax(), inplace=True)
data['Fare'].fillna(data['Fare'].median(skipna=True), inplace=True)

data['Survived']=(data['Survived']=='Yes').astype(int)

print 'Check if there are any missing values:'
print data.isnull().sum()
print '-'*50

cat_vars = ['Sex', 'Port', 'Passenger_Class']

for var in cat_vars:
    # cat_list='var'+'_'+var
    cat_list = pd.get_dummies(data[var], prefix=var)
    data1 = data.join(cat_list)
    data = data1

# "-1" coding
data['PC1'] = data.apply(
    lambda row: int(-1) if row['Passenger_Class_3'] == 1 else int(row['Passenger_Class_1']),
    axis=1
)
data['PC2'] = data.apply(
    lambda row: int(-1) if row['Passenger_Class_3'] == 1 else int(row['Passenger_Class_2']),
    axis=1
)
data['Female'] = data.apply(
    lambda row: int(1) if row['Sex_female'] == 1 else int(-1),
    axis=1
)
data['C'] = data.apply(
    lambda row: int(-1) if row['Port_S'] == 1 else int(row['Port_C']),
    axis=1
)
data['Q'] = data.apply(
    lambda row: int(-1) if row['Port_S'] == 1 else int(row['Port_Q']),
    axis=1
)

data.drop('Port_C', 1, inplace=True)  # redundant
data.drop('Port_Q', 1, inplace=True)  # redundant
data.drop('Port_S', 1, inplace=True)  # redundant

data.drop('Sex_female', 1, inplace=True)  # redundant
data.drop('Sex_male', 1, inplace=True)  # redundant

data.drop('Passenger_Class_1', 1, inplace=True)  # redundant
data.drop('Passenger_Class_2', 1, inplace=True)  # redundant
data.drop('Passenger_Class_3', 1, inplace=True)  # redundant

data.drop('Passenger_Class', 1, inplace=True)  # redundant
data.drop('Sex', 1, inplace=True)  # redundant
data.drop('Port', 1, inplace=True)  # redundant

data = data.rename(columns={
    'C': 'Port_C',
    'Q': 'Port_Q',
    'Female': 'Sex_female',
    'PC1': 'Passenger_Class_1',
    'PC2': 'Passenger_Class_2'})
print data.head()

X = data[data.columns[1:]]
y = data['Survived']

y = label_binarize(y, classes=[0, 1])
n_classes = y.shape[1]
print y, n_classes
# n_classes = 1

# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.3, random_state=0)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.3)

classifier = DecisionTreeClassifier()

y_score = classifier.fit(X_train, y_train).predict(X_test)

fpr = dict()
tpr = dict()
roc_auc = dict()
for i in range(n_classes-1):
    fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
    roc_auc[i] = auc(fpr[i], tpr[i])

# Compute micro-average ROC curve and ROC area
fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

#ROC curve for a specific class here for the class 2
print roc_auc

predictors=['Sex_female', 'Age', 'Siblings_and_Spouses', 'Passenger_Class_1',
            'Parents_and_Children', 'Fare', 'Port_C', 'Port_Q', 'Passenger_Class_2']
dt = DecisionTreeClassifier(criterion='gini', min_samples_split=200, min_impurity_split=0.25, min_samples_leaf=20)
dt = dt.fit(data[predictors], y)
export_graphviz(dt, feature_names=predictors, out_file='DataFiles/treeROC.dot', filled=True)
dot_data = export_graphviz(dt, feature_names=predictors, out_file=None, filled=True)
graph = pydotplus.graph_from_dot_data(dot_data)
# (graph,) = pydot.graph_from_dot_data(dot_data)


system("e:\Programs\Graphviz\\bin\dot.exe -Tpng DataFiles/treeROC.dot -o DataFiles/treeROC.png")


