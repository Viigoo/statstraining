# coding=utf-8
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.tree import DecisionTreeClassifier
from scipy import interp
import pandas as pd
import csv
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import scipy.stats
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.tree import export_graphviz
from os import system
import pydot
import pydotplus
import collections

np.random.seed(12225)
print 'Simple splitting tree example and visualisation'
print '-'*50

data = pd.DataFrame()

# data['x1'] = [0, 0.1, 0.2, 0.3, 0.6, 0.7, 0.9, 0.9, 1]
# data['x2'] = [0, 0.3, 0.1, 0.2, 0.7, 0.8, 0.8, 0.9, 1]
# data['x1'] = [18, 26,  4, 25, 42, 22, 25, 42, 26, 25]
# data['x2'] = [28, 45, 32, 54, 33, 99, 66, 16, 42, 17]
data['x1'] = np.random.random_sample(1000)
data['x2'] = np.random.random_sample(1000)
constant = 0.04
data['y'] = data.apply(
    lambda row: 1 if (row['x1']+np.random.normal(0, constant) > 0.4) & (row['x2']+np.random.normal(0, constant) > 0.6) |
                     (row['x1']+np.random.normal(0, constant) < 0.2) & (row['x2']+np.random.normal(0, constant) < 0.6) else 0,
    axis=1
)
data['color'] = data.apply(
    lambda row: '#0074FFaa' if row['y'] == 1 else '#BB5D00aa',
    axis=1
)
data['marker'] = data.apply(
    lambda row: 'x' if row['y'] == 1 else 'o',
    axis=1
)
print data.head()

dt = DecisionTreeClassifier(criterion='gini', min_samples_split=100, min_impurity_split=0.3,
                            min_samples_leaf=20, class_weight=None)
dt = dt.fit(data[['x1', 'x2']], data['y'])

predictors = ['x1', 'x2']
print '-'*50
print 'To dot file'
with open('DataFiles/splitter.dot', 'w') as dotfile:
    export_graphviz(dt, out_file=dotfile, feature_names=predictors, filled=True)
dotfile.close()

system("e:\Programs\Graphviz\\bin\dot.exe -Tpng DataFiles/splitter.dot -o DataFiles/splitter.png")
(graph,) = pydot.graph_from_dot_file('DataFiles/splitter.dot')



fig = plt.figure(figsize=(9, 9), dpi=100, edgecolor='k', frameon=True)
# plt.scatter(data['x1'], data['x2'], marker='+', s=60, c=data['color'])
for s, c, x, y in zip(data['marker'], data['color'], data['x1'], data['x2']):
    plt.scatter(x, y, s=160, marker=s, c=c)
    # plt.scatter(x, y, s=80, marker=s, c='b')
plt.plot([0, 1], [0.6633, 0.6633], 'k--', lw=3, alpha=0.75)
plt.plot([0.2101, 0.2101], [0, 0.6633], 'k--', lw=3, alpha=0.75)
plt.plot([0.4144, 0.4144], [0.6633, 1], 'k--', lw=3, alpha=0.75)
plt.grid(alpha=0.4)
# plt.title('Example dataset with binary response variable', size=20)
plt.xlabel('x1', fontsize=20, labelpad=8)
plt.ylabel('x2', fontsize=20, labelpad=8)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim(0, 1)
plt.ylim(0, 1)
plt.show()
