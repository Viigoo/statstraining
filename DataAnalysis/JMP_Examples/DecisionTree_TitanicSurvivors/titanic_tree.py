# coding=utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from os import system
import pydot
import pydotplus
import collections
from sklearn.metrics import roc_curve, auc

np.random.seed(123)
print 'Titanic decision tree example'
print '-'*50

# read data from .csv file and put it into content array as strings
file_name = 'StatsFiles/Titanic.csv'
data = pd.read_csv(file_name)
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data

print 'Loading completed!'
print '-'*50
print '-'*50

# Understanding the data...
# ------------------------------------------------------------
data.drop('Home_Destination', 1, inplace=True)
data.drop('Name', 1, inplace=True)

data["Age"].fillna(data["Age"].median(skipna=True), inplace=True)
data['Port'].fillna(data['Port'].value_counts().idxmax(), inplace=True)
data['Fare'].fillna(data['Fare'].median(skipna=True), inplace=True)

data['Survived']=(data['Survived']=='Yes').astype(int)

print 'Check if there are any missing values:'
print data.isnull().sum()
print '-'*50

# Example...
# ------------------------------------------------------------
print 'Data after inserting missing values and dropping unnecessery columns:\n'
print data
# print data['Survived'].unique()

cat_vars = ['Sex', 'Port']
for var in cat_vars:
    # cat_list='var'+'_'+var
    cat_list = pd.get_dummies(data[var], prefix=var)
    data1 = data.join(cat_list)
    data = data1

data.drop('Sex', 1, inplace=True)
data.drop('Sex_male', 1, inplace=True)
data.drop('Port', 1, inplace=True)

print 'Data after creating dummy variables:\n'
print data

# selecting predictors
colnames = data.columns.values.tolist()
predictors=['Passenger_Class', 'Sex_female', 'Age', 'Siblings_and_Spouses',
            'Parents_and_Children', 'Fare', 'Port_C', 'Port_Q', 'Port_S']
# predictors = ['Passenger_Class', 'Age', 'Siblings_and_Spouses',
#             'Parents_and_Children', 'Fare']
target = ['Survived']
print 'All column names   :', colnames
print 'Selected predictors:', predictors
print 'Target:             ', target
print '-'*50, '\n'

# splitting to test and train
data['is_train'] = np.random.uniform(0, 1, len(data)) <= 0.7
train, test = data[data['is_train'] == True], data[data['is_train'] == False]
print 'Data split into train and test (last column is_train):\n'
print train
print test

print '-'*50
print 'Actual decision tree fit attempt:'
# add random_state=99 parameter for reproducibility
dt = DecisionTreeClassifier(criterion='gini', min_samples_split=20, min_impurity_split=0.25,
                            min_samples_leaf=50, class_weight=None)
# dt = DecisionTreeClassifier(criterion='gini', min_samples_split=10, min_impurity_split=0.25,
#                             min_samples_leaf=2, class_weight=None)
# dt = DecisionTreeClassifier(criterion='gini', min_samples_split=200, min_samples_leaf=25)
# dt = DecisionTreeClassifier()
# dt = DecisionTreeClassifier(criterion='gini', min_impurity_split=0.35, max_depth=3, class_weight=None)
print 'feature importances:'
dt = dt.fit(train[predictors], train[target])
for name, importance in zip(predictors, dt.feature_importances_):
    print ('%-20s: %.3f' % (name, importance))

preds = dt.predict(test[predictors])
probs = dt.predict_proba(test[predictors])
tree = pd.crosstab(test['Survived'],preds,rownames=['Actual'],colnames=['Predictions'])
print '\ntree\n'
print tree
print '\npreds\n'
# print preds
print '\nprobs\n'
# print probs
print len(probs)
test['pred'] = preds
test['proba'] = [row[1] for row in probs]
test['is_correct'] = test.apply(
    lambda row: int(1) if row['Survived'] == row['pred'] else int(0),
    axis=1
)
test['TPR'] = test.apply(
    lambda row: int(1) if ((row['Survived'] == 1) and (row['pred'] == 1)) else int(0),
    axis=1
)
test['FPR'] = test.apply(
    lambda row: int(1) if ((row['Survived'] == 0) and (row['pred'] == 1)) else int(0),
    axis=1
)
print tree
tpr = test['TPR'].sum()/(float(test['Survived'].sum()))
fpr = test['FPR'].sum()/(len(test) - float(test['Survived'].sum()))
print test.head(10)
print 'len - tpr - fpr'
print len(test), tpr, fpr
print test['proba'].unique()
print 'correct ratio:', test['is_correct'].sum()/float(len(test))

####################################
# The optimal cut off would be where tpr is high and fpr is low
# tpr - (1-fpr) is zero or near to zero is the optimal cut off point
####################################
fpr, tpr, thresholds = roc_curve(test['Survived'], test['pred'])
roc_auc = auc(fpr, tpr)
i = np.arange(len(tpr)) # index for df
roc = pd.DataFrame({'fpr' : pd.Series(fpr, index=i),'tpr' : pd.Series(tpr, index = i), '1-fpr' : pd.Series(1-fpr, index = i), 'tf' : pd.Series(tpr - (1-fpr), index = i), 'thresholds' : pd.Series(thresholds, index = i)})
roc.ix[(roc.tf-0).abs().argsort()[:1]]

# # Plot tpr vs 1-fpr automated
# fig, ax = pl.subplots()
# pl.plot(roc['tpr'])
# pl.plot(roc['1-fpr'], color = 'red')
# pl.plot([0, len(roc['tpr'])], [0, 1], '--', color = 'black', alpha = 0.3)
# pl.xlabel('1-False Positive Rate')
# pl.ylabel('True Positive Rate')
# pl.title('Receiver operating characteristic')
# ax.set_xticklabels([])
# pl.grid(True, alpha=0.3)
# pl.xlim(0, len(roc['tpr']))
# pl.ylim(0, 1)
# pl.show()

# ROC manual
# ----------
# preds = dt.predict(test[predictors])
# probs = dt.predict_proba(test[predictors])
# tree = pd.crosstab(test['Survived'],preds,rownames=['Actual'],colnames=['Predictions'])
step = 0.01
temp_threshold = 0
roc_tpr = []
roc_fpr = []
thresholds_sorted = test['proba'].unique()
print thresholds_sorted
thresholds_sorted.sort()
# while temp_threshold < 1:
for temp_threshold in thresholds_sorted:
    # print '\ntree\n'
    # print tree
    # print '\npreds\n'
    # print preds
    # print '\nprobs\n'
    # print probs
    # print len(probs)
    test['pred'] = test.apply(
        lambda row: int(1) if row['proba'] >= temp_threshold else int(0),
        axis=1
    )
    # test['proba'] = [row[1] for row in probs]
    test['is_correct'] = test.apply(
        lambda row: int(1) if row['Survived'] == row['pred'] else int(0),
        axis=1
    )
    test['TPR'] = test.apply(
        lambda row: int(1) if ((row['Survived'] == 1) and (row['pred'] == 1)) else int(0),
        axis=1
    )
    test['FPR'] = test.apply(
        lambda row: int(1) if ((row['Survived'] == 0) and (row['pred'] == 1)) else int(0),
        axis=1
    )
    # print tree
    tpr = test['TPR'].sum()/(float(test['Survived'].sum()))
    fpr = test['FPR'].sum()/(len(test) - float(test['Survived'].sum()))
    roc_tpr.append(tpr)
    roc_fpr.append(fpr)
    # print test.head(10)
    # print 'len - tpr - fpr'
    # print len(test), tpr, fpr
    # print test['proba'].unique()
    print 'thres: %.3f' % temp_threshold,
    print 'correct ratio: %.3f' % (test['is_correct'].sum()/float(len(test)))
    temp_threshold += step
roc_tpr.append(0)
roc_fpr.append(0)
print roc_tpr
print roc_fpr
area = 0
lastx = -1
lasty = -1
for y,x in zip(roc_tpr, roc_fpr):
    if lastx != -1:
        height = np.abs(lastx-x)
        bases = (y+lasty)/2.
        print height
        area += height*bases
    lastx = x
    lasty = y

print 'area under roc curve: %f' % area
fig = plt.figure(figsize=(9, 9), dpi=100, edgecolor='k', frameon=True)
plt.plot(roc_fpr, roc_tpr, lw=3)
plt.plot([0, 1], [0, 1], '--', color='black', alpha=0.3, lw=2)
plt.grid(True, alpha=0.3)
plt.xlabel('FPR', fontsize=20, labelpad=8)
plt.ylabel('TPR', fontsize=20, labelpad=8)
plt.xticks([0, 0.2, 0.4, 0.6, 0.8, 1], fontsize=14)
plt.yticks(fontsize=14)
plt.xlim(0, 1)
plt.ylim(0, 1)
plt.show()








print '-'*50
print 'To dot file'
with open('DataFiles/dtree.dot', 'w') as dotfile:
    export_graphviz(dt, out_file=dotfile, feature_names=predictors)
dotfile.close()

# add dot command to system variables ¯\_(ツ)_/¯
system("e:\Programs\Graphviz\\bin\dot.exe -Tpng DataFiles/dtree.dot -o DataFiles/dtree.png")
(graph,) = pydot.graph_from_dot_file('DataFiles/dtree.dot')

# -------------------------------------------------
# Visualize data
export_graphviz(dt, feature_names=predictors, out_file='DataFiles/dtreecolor.dot', filled=True, proportion=False)
dot_data = export_graphviz(dt, feature_names=predictors, out_file=None, filled=True)
graph = pydotplus.graph_from_dot_data(dot_data)
# (graph,) = pydot.graph_from_dot_data(dot_data)

colors = ('red', 'green')
edges = collections.defaultdict(list)

for edge in graph.get_edge_list():
    edges[edge.get_source()].append(int(edge.get_destination()))

for edge in edges:
    edges[edge].sort()
    for i in range(2):
        dest = graph.get_node(str(edges[edge][i]))[0]
        dest.set_fillcolor(colors[i])


system("e:\Programs\Graphviz\\bin\dot.exe -Tpng DataFiles/dtreecolor.dot -o DataFiles/dtreecolor.png")
# graph.write_png("newTree.png")

