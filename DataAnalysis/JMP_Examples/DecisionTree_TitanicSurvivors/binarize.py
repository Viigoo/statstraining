# coding=utf-8
# coding=utf-8
import pandas as pd
import csv
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import scipy.stats
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from os import system
import pydot
import pydotplus
import collections
import sklearn.preprocessing as pre

print 'Titanic decision tree example'
print '-'*50

# read data from .csv file and put it into content array as strings
file_name = 'StatsFiles/Titanic.csv'
data = pd.read_csv(file_name)
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)
print data

print 'Loading completed!'
print '-'*50
print '-'*50

# Understanding the data...
# ------------------------------------------------------------
data.drop('Home_Destination', 1, inplace=True)
data.drop('Name', 1, inplace=True)

data["Age"].fillna(data["Age"].median(skipna=True), inplace=True)
data['Port'].fillna(data['Port'].value_counts().idxmax(), inplace=True)
data['Fare'].fillna(data['Fare'].median(skipna=True), inplace=True)

data['Survived']=(data['Survived']=='Yes').astype(int)

print 'Check if there are any missing values:'
print data.isnull().sum()
print '-'*50

# Example...
# ------------------------------------------------------------
print 'Data after inserting missing values and dropping unnecessery columns:\n'
print data
# print data['Survived'].unique()

# selecting predictors
colnames = data.columns.values.tolist()
# predictors=['Passenger_Class', 'Sex', 'Age', 'Siblings_and_Spouses',
#             'Parents_and_Children', 'Fare', 'Port']
predictors = ['Passenger_Class', 'Age', 'Siblings_and_Spouses',
            'Parents_and_Children', 'Fare']
target = ['Survived']
print 'All column names   :', colnames
print 'Selected predictors:', predictors
print 'Target:             ', target
print '-'*50, '\n'

# splitting to test and train
data['is_train'] = np.random.uniform(0, 1, len(data)) <= 0.7
train, test = data[data['is_train'] == True], data[data['is_train'] == False]
print 'Data split into train and test (last column is_train):\n'
print train
print test

print '-'*50
print 'Actual decision tree fit attempt:'
# add random_state=99 parameter for reproducibility
dt = DecisionTreeClassifier(criterion='gini', min_samples_split=200, min_impurity_split=0.2)
# dt = DecisionTreeClassifier(criterion='gini', min_samples_split=200, min_samples_leaf=25)
# dt = DecisionTreeClassifier()
dt = dt.fit(train[predictors], train[target])

preds = dt.predict(test[predictors])
tree = pd.crosstab(test['Survived'],preds,rownames=['Actual'],colnames=['Predictions'])
print tree

print '-'*50
print 'To dot file'
with open('DataFiles/binarize.dot', 'w') as dotfile:
    export_graphviz(dt, out_file=dotfile, feature_names=predictors, filled=True)
dotfile.close()

# add dot command to system variables ¯\_(ツ)_/¯
system("e:\Programs\Graphviz\\bin\dot.exe -Tpng DataFiles/binarize.dot -o DataFiles/binarize.png")
(graph,) = pydot.graph_from_dot_file('DataFiles/binarize.dot')

