# coding=utf-8
import pandas as pd
import csv
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import scipy.stats
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from os import system
import pydot
import pydotplus
import collections
import pylab as pl
from sklearn.metrics import roc_curve, auc
from sklearn import preprocessing

# read data from .csv file and put it into content array as strings
file_name = 'StatsFiles/Titanic.csv'
data = pd.read_csv(file_name)
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 1000)

data["Age"].fillna(data["Age"].median(skipna=True), inplace=True)
data['Port'].fillna(data['Port'].value_counts().idxmax(), inplace=True)
data['Fare'].fillna(data['Fare'].median(skipna=True), inplace=True)
print data

cat_vars = ['Sex', 'Port']
for var in cat_vars:
    cat_list='var'+'_'+var
    cat_list = pd.get_dummies(data[var], prefix=var)
    data1 = data.join(cat_list)
    data = data1
print data

lb = preprocessing.LabelBinarizer()
lb.fit_transform(data['Port'])
print lb.transform(data['Port'])
print lb.transform(data['Port'])[:,1]

for variable, i in zip(lb.classes_, range(3)):
    data[variable] = lb.transform(data['Port'])[:,i]
print data

