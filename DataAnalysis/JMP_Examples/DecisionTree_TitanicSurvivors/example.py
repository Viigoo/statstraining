# coding=utf-8
import pandas as pd
import csv
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf
import scipy.stats
from statsmodels.stats.outliers_influence import variance_inflation_factor
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from os import system
import pydot

filename = 'StatsFiles/iris.csv'
data=pd.read_csv(filename)
print data.head()
print data['Species'].unique()

colnames=data.columns.values.tolist()
predictors=colnames[:4]
target=colnames[4]
print colnames
print predictors
print target

data['is_train'] = np.random.uniform(0, 1, len(data)) <= .75
train, test = data[data['is_train']==True], data[data['is_train']==False]

print '------'
dt = DecisionTreeClassifier(criterion='entropy', min_samples_split=20, random_state=99)
dt.fit(train[predictors], train[target])

preds = dt.predict(test[predictors])
tree = pd.crosstab(test['Species'],preds,rownames=['Actual'],colnames=['Predictions'])
print tree

print '-------'
print 'To dot file'
with open('StatsFiles/dtree.dot', 'w') as dotfile:
    export_graphviz(dt, out_file=dotfile, feature_names=predictors)
dotfile.close()

# add dot command to system variables ¯\_(ツ)_/¯
system("e:\Programs\Graphviz\\bin\dot.exe -Tpng StatsFiles/dtree.dot -o StatsFiles/dtree.png")
(graph,) = pydot.graph_from_dot_file('StatsFiles/dtree.dot')
print '¯\_(ツ)_/¯'