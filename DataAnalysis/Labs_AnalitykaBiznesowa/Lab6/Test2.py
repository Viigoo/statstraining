# coding=utf-8
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statsmodels.api as sm
from sklearn import datasets
import scipy.stats as sci
import re
import sys
import csv
# read data from .csv file and put it into content array as strings
content_raw = []
file_name = 'StatsFiles/icecream.csv'
with open(file_name, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        content_raw.append(row)

# convert values to floats and separate values from titles
content_titles = content_raw.pop(0)
content_floats = []
for row in content_raw:
    row_float = []
    for element in row:
        row_float.append(float(element.replace(',', '.')))
    content_floats.append(row_float)

consumption = []
temperature = []
price = []
for row in content_floats:
    consumption.append(row[1])
    price.append(row[2])
    temperature.append(row[3])

y = consumption

x = [
     price,
     temperature,
     ]

def reg_m(y, x):
    ones = np.ones(len(x[0]))
    X = sm.add_constant(np.column_stack((x[0], ones)))
    for ele in x[1:]:
        X = sm.add_constant(np.column_stack((ele, X)))
    results = sm.OLS(y, X).fit()
    return results

print reg_m(y, x).summary()
