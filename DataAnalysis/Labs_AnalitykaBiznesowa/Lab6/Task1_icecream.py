# coding=utf-8
import re
import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sci
import sklearn.feature_selection
import statsmodels.api as sm

print 'Analityka Biznesowa - Lab6 - "Liniowa regresja wielokrotna"'
print 'Task 1 - icecream\n'

# read data from .csv file and put it into content array as strings
content_raw = []
file_name = 'StatsFiles/icecream.csv'
with open(file_name, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        content_raw.append(row)

# convert values to floats and separate values from titles
content_titles = content_raw.pop(0)
content_floats = []
for row in content_raw:
    row_float = []
    for element in row:
        row_float.append(float(element.replace(',', '.')))
    content_floats.append(row_float)

consumption = []
temperature = []
price = []
for row in content_floats:
    consumption.append(row[1])
    price.append(row[2])
    temperature.append(row[3])

# (a) Zaimportować plik icecream.sas7bdat i wyświetlić jego zawartość.
# display data array, ready to be processed
print '\tTask (a):', file_name
print '/', '-'*59, '\\'
for title in content_titles:
    print '|{:^13s}|'.format(title),
print ''
print '|', '-'*59, '|'
for row in content_floats:
    for idx, element in enumerate(row):
        print '|    {:<9g}|'.format(element),
    print ''
print '\\', '-'*59, '/'

# (b)
print '\tTask (b)'
r_pc, r_pc_pval = sci.pearsonr(price, consumption)
r_tc, r_tc_pval = sci.pearsonr(temperature, consumption)

fig = plt.figure(figsize=(14, 8), dpi=100, facecolor='#aaaaaaff')
fig.suptitle("Lab6 - Task 1b: What is more impactful?", fontsize=24)

plt.subplot(121)
plt.title('Price vs. Consumption, Pearson coeff = %.3f' % r_pc)
fit = np.polyfit(price, consumption, 1)
fit_fn = np.poly1d(fit)
plt.plot(price, consumption, 'go', price, fit_fn(price), '-k')
plt.grid(True)
plt.xlabel('Price')
plt.ylabel('Consumption')

plt.subplot(122)
plt.title('Temperature vs. Consumption, Pearson coeff = %.3f' % r_tc)
fit = np.polyfit(temperature, consumption, 1)
fit_fn = np.poly1d(fit)
plt.plot(temperature, consumption, 'ro', temperature, fit_fn(temperature), '-k')
plt.grid(True)
plt.xlabel('Temperature')
plt.ylabel('Consumption')
# plt.show()
print 'Pearson coefficients for Price vs. Consumption and Temperature ' \
      'vs. Consumption are respectively: %.3f and %.3f, which indicates that ' \
      'temperature is more impactful than price on consumption' % (r_pc, r_tc)

# (c)
print '\tTask (c)'
plt.close('all')
def perform_t1c(x, y, alpha=0.95, plot_title=''):
    z = np.polyfit(x, y, 1)
    p = np.poly1d(z)
    fit = p(x)
    y_err = y - fit

    c_limit = alpha
    mean_x = np.mean(x)
    n = len(x)  # number of samples in origional fit
    tstat = sci.t.ppf(c_limit, n - 1)  # appropriate t value
    s_err = np.sum(np.power(y_err, 2))  # sum of the squares of the residuals

    # create series of new test x-values to predict for
    p_x = np.linspace(np.min(x), np.max(x), 50)
    confs = tstat * np.sqrt(
        (s_err / (n - 2)) * (1.0 / n + (np.power((p_x - mean_x), 2) /
                                        ((np.sum(np.power(x, 2))) - n * (
                                        np.power(mean_x, 2))))))

    p_y = p(p_x)
    lower = p_y - abs(confs)
    upper = p_y + abs(confs)

    fit0 = np.polyfit(x, y, 0)
    fit_fn0 = np.poly1d(fit0)

    plt.title(plot_title)
    plt.plot(x, y, 'rx')
    plt.plot(x, fit_fn0(x), 'k-', alpha=0.4)
    plt.plot(p_x, p_y, 'r-')
    plt.plot(p_x, lower, 'k--')
    plt.plot(p_x, upper, 'k--')
    plt.grid(True, alpha=0.5)
    # plt.show()

def perform_rest_histogram(x, y, b=9):
    z = np.polyfit(x, y, 1)
    p = np.poly1d(z)
    fit = p(x)
    y_err = y - fit
    plt.title("Distribution of errors sum=(%f)" % np.sum(np.abs(y_err)))
    plt.hist(y_err, bins=b, edgecolor='black')

fig = plt.figure(figsize=(14, 8), dpi=100, facecolor='#aaaaaaff')
fig.suptitle("Lab6 - Task 1c", fontsize=24)

plt.subplot(221)
perform_t1c(price, consumption, plot_title='Price')
plt.subplot(222)
perform_t1c(temperature, consumption, plot_title='Temperature')
plt.subplot(223)
perform_rest_histogram(price, consumption, 7)
plt.subplot(224)
perform_rest_histogram(temperature, consumption, 5)
plt.show()

y = consumption

# Coefficients appear in summary in reversed order:
# x1 = temperature
# x2 = price
x = [price, temperature]

def reg_m(y, x):
    ones = np.ones(len(x[0]))
    X = sm.add_constant(np.column_stack((x[0], ones)))
    for ele in x[1:]:
        X = sm.add_constant(np.column_stack((ele, X)))
    results = sm.OLS(y, X).fit()
    return results

print reg_m(y, x).summary()

print """\nBecause 0 degree fit (fit independent from X) on the left hand side
plot is fully placed between 95% confidence intervals - that means that F prob
is higher than 0.05 (as opposed to temperature - where F prob. <0.0001"""
print """\nSign of factors determines how an independent variable is correlated
with dependent variable. Positive sign indicates that along with the growth of
a independent variable - dependent one is increasing as well (negative sign means
the opposite). Factors of variables near value of 0 mean that those variables
have very little impact on the dependent variable."""
print """\nCoeff. of temperature (x1) is 0.0030 (conf. interval 0.002 to 0.004)
and that means that temperature has positive influence on consumption"""
print """Coeff. of price (x2) is -1.4018 (conf. interval -3.3 to 0.496).
Although coeff. of price looks "stronger" than coeff. of temperature, its
confidence interval contains 0, which means that it is likely that price factor
had minor or no effect at all (Prob (F-statistic) = 0.16)"""
