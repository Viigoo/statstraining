# coding=utf-8
import re
import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sci
import sklearn.feature_selection
import statsmodels.api as sm

print 'Analityka Biznesowa - Lab6 - "Liniowa regresja wielokrotna"'
print 'Task 2 - clouds\n'

# read data from .csv file and put it into content array as strings
content = []
file_name = 'StatsFiles/cloud.csv'
with open(file_name, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        content.append(row)

# convert values to floats and separate values from titles
content_titles = content.pop(0)
content_floats = []
for row in content:
    row_float = []
    for element in row:
        row_float.append(float(element.replace(',', '.')))
    content_floats.append(row_float)

seeding = []
time = []
sne = []
cloudcover = []
prewetness = []
echomotion = []
rainfall = []
for row in content_floats:
    seeding.append(row[0])
    time.append(row[1])
    sne.append(row[2])
    cloudcover.append(row[3])
    prewetness.append(row[4])
    echomotion.append(row[5])
    rainfall.append(row[6])

# display data array, ready to be processed
print '\tTask:', file_name
print '/', '-'*86, '\\'
for title in content_titles:
    print '|{:^10s}|'.format(title),
print ''
print '|', '-'*86, '|'
for row in content_floats:
    for idx, element in enumerate(row):
        print '|{:>8.3f}  |'.format(element),
    print ''
print '\\', '-'*86, '/'

# (a)
print '\tTask (a) - boxplots'
# dependent variable - rainfall
rainfall_seeding_0 = []
rainfall_seeding_1 = []
rainfall_echomotion_1 = []
rainfall_echomotion_2 = []

# create rainfall subarray when seeding is 0 or 1
# and when echomotion is 1 or 2
for i in range(len(content_floats)):
    if (seeding[i] < 0.5):
        rainfall_seeding_0.append(rainfall[i])
    else:
        rainfall_seeding_1.append(rainfall[i])
    if (echomotion[i] < 1.5):
        rainfall_echomotion_1.append(rainfall[i])
    else:
        rainfall_echomotion_2.append(rainfall[i])
databox_seeding = [rainfall_seeding_0, rainfall_seeding_1]
databox_echomotion = [rainfall_echomotion_1, rainfall_echomotion_2]

fig = plt.figure(figsize=(14, 8), dpi=100, facecolor='#aaaaaaff')
fig.suptitle("Lab6 - Task 2a: Boxplots", fontsize=24)

# boxplots
plt.subplot(121)
plt.title('Rainfall vs. Seeding boxplot')
plt.xlabel('Seeding'); plt.ylabel('Rainfall')
plt.yticks(range(0, 15, 1))
plt.grid(visible=True, axis='y', alpha=0.5)
plt.boxplot(databox_seeding, whiskerprops=dict(linestyle='--'), widths=0.5,
                       labels=['No (0)', 'Yes (1)'])
plt.subplot(122)
plt.title('Rainfall vs. Echomotion boxplot')
plt.xlabel('Echomotion'); plt.ylabel('Rainfall')
plt.yticks(range(0, 15, 1))
plt.grid(visible=True, axis='y', alpha=0.5)
plt.boxplot(databox_echomotion, whiskerprops=dict(linestyle='--'), widths=0.5,
                       labels=['Moving (1)', 'Motionless (2)'])
plt.show()

# scatterplots
plt.close('all')
def perform_t1c(x, y, alpha=0.95, plot_title=''):
    z = np.polyfit(x, y, 1)
    p = np.poly1d(z)
    fit = p(x)
    y_err = y - fit

    c_limit = alpha
    mean_x = np.mean(x)
    n = len(x)  # number of samples in origional fit
    tstat = sci.t.ppf(c_limit, n - 1)  # appropriate t value
    s_err = np.sum(np.power(y_err, 2))  # sum of the squares of the residuals

    # create series of new test x-values to predict for
    p_x = np.linspace(np.min(x), np.max(x), 50)
    confs = tstat * np.sqrt(
        (s_err / (n - 2)) * (1.0 / n + (np.power((p_x - mean_x), 2) /
                                        ((np.sum(np.power(x, 2))) - n * (
                                        np.power(mean_x, 2))))))

    p_y = p(p_x)
    lower = p_y - abs(confs)
    upper = p_y + abs(confs)

    fit0 = np.polyfit(x, y, 0)
    fit_fn0 = np.poly1d(fit0)

    # plt.title(plot_title)
    plt.plot(x, y, 'rx', alpha=0.5)
    plt.plot(x, fit_fn0(x), 'k-', alpha=0.6)
    plt.plot(p_x, p_y, 'r-')
    plt.plot(p_x, lower, 'b--', alpha=0.6)
    plt.plot(p_x, upper, 'b--', alpha=0.6)
    plt.ylabel('rainfall')
    plt.xlabel(plot_title)
    plt.grid(True, alpha=0.5)

fig = plt.figure(figsize=(14, 8), dpi=100, facecolor='#aaaaaaff')
fig.suptitle("Lab6 - Task 2a: Scatterplots", fontsize=24)
plt.subplot(221)
perform_t1c(time, rainfall, plot_title='Time')
plt.subplot(222)
perform_t1c(sne, rainfall, plot_title='SNE')
plt.subplot(223)
perform_t1c(cloudcover, rainfall, plot_title='Cloudover')
plt.subplot(224)
perform_t1c(prewetness, rainfall, plot_title='Prewetness')
plt.show()