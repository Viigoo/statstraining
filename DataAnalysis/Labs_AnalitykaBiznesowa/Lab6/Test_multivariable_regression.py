# coding=utf-8
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statsmodels.api as sm
from sklearn import datasets ## imports datasets from scikit-learn
# print data.DESCR
# print data.target
# x1 = [1, 4, 9, 16, 25]
# x2 = [1, 2, 4,  8, 16]
# y  = [1, 3, 8, 12, 20]

data = datasets.load_boston() ## loads Boston dataset from datasets library
# define the data/predictors as the pre-set feature names
df = pd.DataFrame(data.data, columns=data.feature_names)

# Put the target (housing value -- MEDV) in another DataFrame
target = pd.DataFrame(data.target, columns=["MEDV"])

X = df[["RM", "LSTAT"]]
# X = df[["RM", "LSTAT"]]
print type(X)
y = target["MEDV"]

# Note the difference in argument order
model = sm.OLS(y, X).fit()
predictions = model.predict(X) # make the predictions by the model

# Print out the statistics
print model.summary()