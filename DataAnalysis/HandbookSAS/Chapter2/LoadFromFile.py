# -*- coding: utf-8 -*-
import re, sys

def get_data_array_from_file(file_name=''):
    """
    Parses provided file and creates an array containing all elements
    that are matched with regex.

    Parameters
    --------
    file_name : string
        Name of a file that will be converted (if possible) to array of elements

    Returns
    --------
    data : 2-D array
        Array containing all matching elements. Each element is also an array of variables
    """
    print 'File loader started...'
    if not file_name:
        print 'No file selected, aborting...'
        return
    fname = file_name
    with open(fname) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    # content = [x.strip() for x in content]
    content = [x.strip('\n') for x in content]

    regex = "^(\*|\s)([a-zA-z\s]+?)\s+(\d+)\s+(\d+)"
    data = []
    not_found = 0

    for line in content:
        try:
            matchObj = re.match(regex, line)

            location = 'N' if matchObj.group(1) == '*' else 'S'
            city_name = matchObj.group(2)
            mortality = int(matchObj.group(3))
            hardness = int(matchObj.group(4))

            data.append((location, city_name, mortality, hardness))
        except AttributeError:
            not_found += 1
            # print 'Line', content.index(line) + 1, 'not matching: ', line
    print 'File loaded successfully.'
    return data
sys.stdout.flush()
