# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

print 'Histogram Very Basic Test'
data = [1.5, 1.5, 1.5, 1.5, 2.1, 2.3, 2.8, 4.2, 4.3, 7.4]
# bins_amount = int(np.floor(np.sqrt(len(data))))
bins_amount = 5
n, bins, patches = plt.hist(data, normed=1, facecolor='#0000ff50', edgecolor='black', linewidth='1.5', bins=bins_amount)

print n
print bins
print patches

plt.title('Basic histogram test')
plt.xlabel('Values')
plt.ylabel('Amount')
plt.xticks(bins, bins)
plt.margins(0.2)
plt.grid(False)
# plt.xlim(xmin=0, xmax=7.9)
# plt.ylim(ymin=0, ymax=(np.max(n)+2))
my_pdf = scipy.stats.gaussian_kde(data)
x = np.linspace(-2, 10, 100)
plt.plot(x, my_pdf(x))
plt.show()
