# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import scipy.stats
import matplotlib.pyplot as plt
import seaborn as sns

data = np.random.normal(size=100)
x = np.linspace(0, 5, 100)
# std_dev = np.std(data, ddof=1)
# mean = np.mean(data)
std_dev = 1
mean = 0

# log normal distribution - seems ok according to wiki
# distlog = scipy.stats.lognorm([1], loc=0, scale=mean) # SCALE = MEAN!
distlog = scipy.stats.lognorm([1], loc=0)
plt.plot(x, distlog.pdf(x), 'c--', alpha=0.4, label='Lognorm \'pdf\', o`=1, u=0')
distlog = scipy.stats.lognorm([0.5], loc=0)
plt.plot(x, distlog.pdf(x), 'm--', alpha=0.4, label='Lognorm \'pdf\', o`=0.5, u=0')
distlog = scipy.stats.lognorm([0.25], loc=0)
plt.plot(x, distlog.pdf(x), 'y--', alpha=0.7, label='Lognorm \'pdf\', o`=0.25, u=0')

# exponential distribution - seems ok according to wiki (scale = 1/lambda)
distexp_pdf = scipy.stats.expon.pdf(x, scale=2)
plt.plot(x, distexp_pdf, 'r--', lw=3, alpha=0.4, label='Expon \'pdf\', lambda=0.5')
distexp_pdf = scipy.stats.expon.pdf(x, scale=1)
plt.plot(x, distexp_pdf, 'g--', lw=3, alpha=0.4, label='Expon \'pdf\', lambda=1')
distexp_pdf = scipy.stats.expon.pdf(x, scale=2./3)
plt.plot(x, distexp_pdf, 'b--', lw=3, alpha=0.4, label='Expon \'pdf\', lambda=1.5')

plt.legend()
plt.show()
