import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
np.random.seed(12345678)
# np.random.seed(133)
# x = np.random.random(10)
# y = np.random.random(10)
x = [1, 2, 3, 4, 5]
y = [5, 6, 5, 8, 7.4]
# print x
# print y

# To get coefficient of determination (r_squared)
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
print("r-squared:", r_value**2)
print("intercept:", intercept)
print("slope:", slope)

# make line for every point... 2/10
y_r = []
for i in range(len(x)):
    y_r.append(intercept + slope*x[i])
# plt.plot(x, y_r, 'r--', label='version1')

# make line for start and end
def range(arr):
    return (max(arr) - min(arr))
x_reg_start = min(x) - range(x)*0.1
x_reg_end = max(x) + range(x)*0.1
x_reg = [x_reg_start, x_reg_end]
y_reg = [intercept + slope*x_reg_start, intercept + slope*x_reg_end]
plt.plot(x_reg, y_reg, 'r--', label='version2')

# Plot the data along with the fitted line
plt.plot(x, y, 'ko', label='original data')

# plt.plot(x, intercept + slope*x, 'r', label='fitted line')
plt.legend()
plt.show()