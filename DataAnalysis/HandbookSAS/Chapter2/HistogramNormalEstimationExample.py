# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

print 'Histogram Very Basic Test'
# data = [1.5, 1.5, 1.5, 1.5, 2.1, 2.3, 2.8, 4.2, 4.3, 7.4]
data = np.random.normal(0, 1000, 1000)

# bins_amount = 5
n, bins, patches = plt.hist(data, normed=1, facecolor='#0000ff50', edgecolor='black', linewidth='1.5')

mean = np.mean(data)
std_dev = np.std(data, ddof=1)
print mean
print std_dev

plt.title('Basic histogram test')
plt.xlabel('Values')
plt.ylabel('Amount')
plt.xticks(bins, bins)
plt.margins(0.2)
plt.grid(False)
# plt.xlim(xmin=1, xmax=7.9)
# plt.ylim(ymin=0, ymax=(np.max(n)+ 0.1))

# kernel distribution function
my_pdf = scipy.stats.gaussian_kde(data)
x = np.linspace(min(data), max(data), 300)
plt.plot(x, my_pdf(x), 'r--')

# normal distribution estimation
x_axis = np.linspace(min(data), max(data), 300)
plt.plot(x_axis, scipy.stats.norm.pdf(x_axis, mean, std_dev))

plt.show()
