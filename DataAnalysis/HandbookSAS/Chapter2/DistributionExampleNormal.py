# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats

import LoadFromFile

x = np.array([0, 1, 2, 3, 4.5, 5])
y = np.array([2, 8, 3, 7,   8, 1])

# y should be sorted for both of these methods
order = y.argsort()
y = y[order]
x = x[order]

def what_is_x_when_y_is(input, x, y):
    return x[y.searchsorted(input, 'left')]

def interp_x_from_y(input, x, y):
    return np.interp(input, y, x)

print what_is_x_when_y_is(7, x, y)
# 3
print interp_x_from_y(1.5, x, y)
# 2.5


data_water = LoadFromFile.get_data_array_from_file()
print data_water

hardness_north = []
hardness_south = []
hardness_overall = []
mortality_north = []
mortality_south = []
mortality_overall = []

for data in data_water:
    hardness_overall.append(data[3])
    mortality_overall.append(data[2])
    if data[0] == 'N':
        mortality_north.append(data[2])
        hardness_north.append(data[3])
    else:
        mortality_south.append(data[2])
        hardness_south.append(data[3])

# data = np.random.normal(size=100, loc=0, scale=1.5)
data = mortality_overall
std_dev = np.std(data, ddof=1)
mean = np.mean(data)
n = len(data)
x = np.linspace(np.min(data), np.max(data), 100000)
title = 'Mean:', mean, 'Std. Dev:', std_dev
plt.title(title)

# kernel density estimation
y_kernel = scipy.stats.gaussian_kde(data, bw_method=0.5)
plt.plot(x, y_kernel(x), 'r.', lw=2, alpha=0.1, label='Kernel density estimation')

# normal distribution
y_norm = scipy.stats.norm.cdf(x, mean, std_dev)
generated_norm = scipy.stats.norm(mean, std_dev)
plt.plot(x, y_norm, 'k.', lw=4, alpha=0.6, label='Normal dist. est.')

# # kernel 1 by 1
# counter = 0
# for i in x:
#     counter += 1
#     print '%4.1f: %.3f' % (i, y_kernel(i)),
#     # plt.scatter(i, y_kernel(i))
#     if counter % 10 == 0:
#         print ''
#
# print '---'
# # normal 1 by 1
# counter = 0
# for i in x:
#     print '%4.1f: %.3f |' % (i, y_norm[counter]),
#     # plt.scatter(i, y_norm[counter])
#     counter += 1
#     if counter % 10 == 0:
#         print ''

print ''
print what_is_x_when_y_is(0.01, x, y_norm), generated_norm.ppf(0.01)
print what_is_x_when_y_is(0.05, x, y_norm), generated_norm.ppf(0.05)
print what_is_x_when_y_is(0.1, x, y_norm), generated_norm.ppf(0.1)
print what_is_x_when_y_is(0.25, x, y_norm), generated_norm.ppf(0.25)
print what_is_x_when_y_is(0.5, x, y_norm), generated_norm.ppf(0.5)
print what_is_x_when_y_is(0.75, x, y_norm), generated_norm.ppf(0.75)
print what_is_x_when_y_is(0.9, x, y_norm), generated_norm.ppf(0.9)
print what_is_x_when_y_is(0.95, x, y_norm), generated_norm.ppf(0.95)
print what_is_x_when_y_is(0.99, x, y_norm), generated_norm.ppf(0.99)

# plt.legend()
# plt.show()
