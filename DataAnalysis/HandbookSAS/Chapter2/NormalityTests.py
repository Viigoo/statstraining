# -*- coding: utf-8 -*-
import scipy.stats
import statsmodels
import sys
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import LoadFromFile

a1 = [30.02, 29.99, 30.11, 29.97, 30.01, 29.99]
a2 = [29.89, 29.93, 29.72, 29.98, 30.02, 29.98]

n1 = len(a1)
n2 = len(a2)
x = range(n1)
mean1 = np.mean(a1)
mean2 = np.mean(a2)
std1 = np.std(a1, ddof=1)
std2 = np.std(a2, ddof=1)

plt.scatter(x, a1, label='a1')
plt.scatter(x, a2, label='a2')
plt.plot([0, 6], [mean1, mean1])
plt.plot([0, 6], [mean2, mean2])
plt.legend()
# plt.show()

print 'mean a1: ', mean1
print 'mean a2: ', mean2
print 'diffrnc: ', np.abs(mean1 - mean2)
print 'std  a1: ', np.round(std1, 2)
print 'std  a2: ', np.round(std2, 2)

print 'sqrt(std^2/n + std^2/n) = ', np.sqrt(((std1**2)/(n1))+((std2**2)/(n2)))
ttest_rel = scipy.stats.ttest_rel(a1, a2)
ttest_ind_equal_var = scipy.stats.ttest_ind(a1, a2, equal_var=True)
ttest_ind_inequal_var = scipy.stats.ttest_ind(a1, a2, equal_var=False)

print 'Equal   Var Statistic:', ttest_ind_equal_var[0], 'p-value:', ttest_ind_equal_var[1]
print 'Inequal Var Statistic:', ttest_ind_inequal_var[0], 'p-value:', ttest_ind_inequal_var[1]
print 'Wikipedia:'
print 'Equal   Var: The test statistic is approx. 1.959, which gives a 2-tailed test p-value of 0.09077.'
print 'Inequal Var: The test statistic is approx. equal to 1.959, which gives 2-sided p-value of 0.07857.'
# print scipy.stats.ttest_1samp(a1, 29.9)
# print scipy.stats.levene(a1, a2)
