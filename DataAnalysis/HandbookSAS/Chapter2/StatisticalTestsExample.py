# -*- coding: utf-8 -*-
import scipy.stats
import numpy as np
import statsmodels.stats.weightstats
import statsmodels.stats.descriptivestats
import statsmodels.stats.diagnostic

test_data_1 = [1, 2, 3, 1, 2, 3]
test_data_2 = [0, 2, 4, 0, 2, 5]
univariate_data = test_data_1 + test_data_2
print test_data_1
print test_data_2
print univariate_data

levene_st, levene_pval = scipy.stats.levene(test_data_1, test_data_2)
ttest_st, ttest_pval = scipy.stats.ttest_ind(test_data_1, test_data_2, axis=0, equal_var=True)
ttest_st_ineqvar, ttest_pval_inequvar = scipy.stats.ttest_ind(test_data_1, test_data_2, axis=0, equal_var=False)
# ttest_rel_st, ttest_rel_pval = scipy.stats.ttest_rel(test_data_1, test_data_2, axis=0)
ttest_st2, ttest_pval2, df2 = statsmodels.stats.weightstats.ttest_ind(test_data_2, test_data_1, usevar='pooled', value=0)
ttest_st3, ttest_pval3, df3 = statsmodels.stats.weightstats.ttest_ind(test_data_2, test_data_1, usevar='unequal', value=0)
ttest_st4, ttest_pval4 = statsmodels.stats.weightstats.ztest(test_data_1, test_data_2, ddof=0)
# ttest_st5, ttest_pval5 =
median_st, median_pval, median_grandvalue, contingency_table = scipy.stats.median_test(test_data_1, test_data_2, ties='below', correction=False, lambda_=4)
kruskal_st, kruskal_pval = scipy.stats.kruskal(test_data_1, test_data_2)
sign_st, sign_pval = statsmodels.stats.descriptivestats.sign_test(univariate_data, mu0=0)
kol_smir_st, kol_smir_pval = scipy.stats.ks_2samp(test_data_1, test_data_2)
kol_smir_st2, kol_smir_pval2 = statsmodels.stats.diagnostic.kstest_normal(univariate_data)
# kol_smir_st, kol_smir_pval = scipy.stats.kstest(test_data_1, 'norm')
shapiro_st, shapiro_pval = scipy.stats.shapiro(univariate_data)
# cramer_mises_st, cramer_mises_pval = scipy.stats.vonmises(univariate_data)
anderson_st, anderson_crit_values, anderson_signific_level = scipy.stats.anderson(univariate_data, dist='norm')
anderson_st2, anderson_pval2 = statsmodels.stats.diagnostic.normal_ad(np.array(univariate_data))
# wilcoxon_st, wilcoxon_pval = scipy.stats.wilcoxon(test_data_1, test_data_2, zero_method='zsplit', correction=False)
wilcoxon_st, wilcoxon_pval = scipy.stats.wilcoxon(univariate_data, univariate_data, zero_method='zsplit')
wilcoxon_st2, wilcoxon_pval2 = scipy.stats.wilcoxon(univariate_data)
# wilcoxon_st, wilcoxon_pval = scipy.stats.ranksums(test_data_1, test_data_2)
print 'Levene    (is variance equal?)                      - Statistic: %g; P-value: %.4f%%' % (levene_st, levene_pval*100)
print 'Student t (is mean equal? if variance is     equal) - Statistic: %g; P-value: %.4f%%' % (ttest_st, ttest_pval*100)
print 'Student t (is mean equal? if variance is NOT equal) - Statistic: %g; P-value: %.4f%%' % (ttest_st_ineqvar, ttest_pval_inequvar*100)
print '(sm)Stud t(is mean equal? if variance is     equal) - Statistic: %g; P-value: %.4f%%' % (ttest_st2, ttest_pval2*100)
print '(sm)Stud t(is mean equal? if variance is NOT equal) - Statistic: %g; P-value: %.4f%%' % (ttest_st3, ttest_pval3*100)
print 'statsmodels - ztest (is mean equal?)                - Statistic: %g; P-value: %.4f%%' % (ttest_st4, ttest_pval4*100)
# print 'Stud t rel(is mean equal? if variance is NOT equal) - Statistic: %g; P-value: %.4f%%' % (ttest_rel_st, ttest_rel_pval*100)
print 'MedianTest(is median equal?)                        - Statistic: %g; P-value: %.4f%%' % (median_st, median_pval*100)
print 'Kruskal   (is median equal?)                        - Statistic: %g; P-value: %.4f%%' % (kruskal_st, kruskal_pval*100)
print 'SignTest M(is median equal?)                        - Statistic: %g; P-value: %.4f%%' % (sign_st, sign_pval*100)
print 'Kolm-Smirn      (is data from normal dist. sample?) - Statistic: %g; P-value: %.4f%%' % (kol_smir_st, kol_smir_pval*100)
print 'Kolm-Smirn StMod(is data from normal dist. sample?) - Statistic: %g; P-value: %.4f%%' % (kol_smir_st2, kol_smir_pval2*100) #ok
print 'Shap-Wilk       (is data from normal dist. sample?) - Statistic: %g; P-value: %.4f%%' % (shapiro_st, shapiro_pval*100) #ok
# print 'Cram-Mises(is data drawn from normal dist. sample?) - Statistic: %g; P-value: %.4f%%' % (cramer_mises_st, cramer_mises_pval*100)
print 'Ander-Darl      (is data from certain dist. sample?)- Statistic: %g; P-value:' % (anderson_st), anderson_crit_values, anderson_signific_level
print 'Ander-Darl StMod(is data from certain dist. sample?)- Statistic: %g; P-value: %.4f%%' % (anderson_st2, anderson_pval2*100) #ok
print 'Wilcoxon  (is mean equal?(non-parametric: no normal)- Statistic: %g; P-value: %g%%' % (wilcoxon_st, wilcoxon_pval*100)
print 'Wilcoxon  (is mean equal?(non-parametric: no normal)- Statistic: %g; P-value: %g%%' % (wilcoxon_st2, wilcoxon_pval2*100)