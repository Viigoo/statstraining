# -*- coding: utf-8 -*-
from time import clock
import scipy
import numpy as np
import matplotlib.pyplot as plt
# np.random.seed(1234)
data = []
data_diff = []
data_mean = []
data_std = []
print np.linspace(0, 1000, 11)
plt.ion()
plt.style.use('dark_background')
# plt.style.use('seaborn-darkgrid')
# plt.rcParams['axes.facecolor'] = 'black'
# print(plt.style.available)
start = clock()

for i in range(10000):
    data.append(np.random.random()**40 * 12)
    data_mean.append(np.mean(data))
    data_std.append(np.std(data))
    data_diff.append(np.cos(i/30.))
    plt.cla()
    plt.plot(range(len(data)), data)
    plt.plot(range(len(data)), data_mean)
    plt.plot(range(len(data)), data_std)
    # plt.plot(range(len(data)), data_diff)
    plt.pause(0.001)
    if (i + 1) % 100 == 0:
        print clock() - start
        start = clock()


while True:
    plt.pause(0.05)
