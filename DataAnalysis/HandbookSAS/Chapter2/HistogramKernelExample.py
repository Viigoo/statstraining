from scipy.stats.kde import gaussian_kde
import scipy.stats
import numpy as np
from pylab import plot,show,hist

# creating data with two peaks
sampD1 = scipy.stats.norm.rvs(loc=-1.0,scale=1,size=300)
sampD2 = scipy.stats.norm.rvs(loc=2.0,scale=0.5,size=300)
# sampD1 = [1, 1, 2, 2, 3, 3, 3, 4]
# sampD2 = [1, 1, 2, 2, 3, 3, 3, 4]
samp = np.hstack([sampD1, sampD2])

print sampD1
# print sampD2
print samp

# obtaining the pdf (my_pdf is a function!)
my_pdf = gaussian_kde(samp)

# plotting the result
x = np.linspace(-5, 5, 100)
plot(x, my_pdf(x), 'r') # distribution function
hist(samp, normed=1, alpha=.3) # histogram
show()

