# -*- coding: utf-8 -*-
from numpy import ptp

def get_x_y_linear_reg_fit(x, intercept, slope):
    x_reg_start = min(x) - ptp(x)*0.1
    x_reg_end = max(x) + ptp(x)*0.1
    x_reg = [x_reg_start, x_reg_end]
    y_reg = [intercept + slope*x_reg_start, intercept + slope*x_reg_end]
    return x_reg, y_reg
