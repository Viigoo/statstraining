# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import scipy.stats
import matplotlib.pyplot as plt
import seaborn as sns

data = np.random.normal(size=100)
x = np.linspace(-5, 5, 100)
np.random.seed(123)
std_dev = np.std(data, ddof=1)
mean = np.mean(data)

# kernel density estimation (seaborn)
sns.set(color_codes=True)
sns.distplot(data, color='#000000FF', label='Seaborn histogram/kernel est.')

# kernel density estimation
x_kernel = scipy.stats.gaussian_kde(data)
plt.plot(x, x_kernel(x), 'r--', label='Kernel density estimation')

# log normal distribution
distlog = scipy.stats.lognorm([std_dev], loc=mean)
plt.plot(x, distlog.pdf(x), 'b-.', label='Lognorm \'pdf\'')
# plt.plot(x, distlog.cdf(x), 'b--', label='Lognorm \'cdf\'')

# normal distribution
plt.plot(x, scipy.stats.norm.pdf(x, mean, std_dev), 'g--', label='Normal dist. est.')

# exponential distribution
distexp_pdf = scipy.stats.expon.pdf(x)
plt.plot(x, distexp_pdf, 'm-', lw=3, alpha=0.6, label='expon \'pdf\'')
# distexp_cdf = scipy.stats.expon.cdf(x)
# plt.plot(x, distexp_cdf, 'm-.', lw=3, alpha=0.6, label='expon \'cdf\'')

# another method
# rv = scipy.stats.expon()
# plt.plot(x, rv.pdf(x), 'm-', lw=2, label='expon frozen pdf')

plt.legend()
plt.show()
