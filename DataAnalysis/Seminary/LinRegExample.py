#!/usr/bin/env python
# encoding=utf8
import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import scipy.stats as sci
import sys

reload(sys)
sys.setdefaultencoding('utf8')

x = [ 1, 2, 3, 4, 5   ]
y = [ 5, 6, 5, 8, 7.4 ]

# To get coefficient of determination (r_squared)
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

x_start = 0
x_end = 6
y_start = intercept + slope*x_start
y_end = intercept + slope*x_end

# Plot the regression line
# plt.plot([x_start, x_end],
#          [y_start, y_end],
#          'r--',
#          label='fitted line')
#
# # Plot the data along with the fitted line
# plt.plot(x, y, 'ko', label='data points')
#
# plt.title('Linear regression example')
# plt.xlabel('Predictors')
# plt.ylabel('Response')
# plt.grid(True, color='#000000', alpha=0.2, linewidth=1.5)
# plt.legend()
# plt.show()


def perform_t1c(x, y, alpha=0.95, plot_title=''):
    z = np.polyfit(x, y, 1)
    p = np.poly1d(z)
    fit = p(x)
    y_err = y - fit

    c_limit = alpha
    mean_x = np.mean(x)
    n = len(x)  # number of samples in origional fit
    tstat = sci.t.ppf(c_limit, n - 1)  # appropriate t value
    s_err = np.sum(np.power(y_err, 2))  # sum of the squares of the residuals

    # create series of new test x-values to predict for
    p_x = np.linspace(np.min(x), np.max(x), 50)
    confs = tstat * np.sqrt(
        (s_err / (n - 2)) * (1.0 / n + (np.power((p_x - mean_x), 2) /
                                        ((np.sum(np.power(x, 2))) - n * (
                                        np.power(mean_x, 2))))))

    p_y = p(p_x)
    lower = p_y - abs(confs)
    upper = p_y + abs(confs)

    fit0 = np.polyfit(x, y, 0)
    fit_fn0 = np.poly1d(fit0)

    plt.title(plot_title, fontsize=26, y=1.02)
    plt.plot(x, y, 'kx', ms=7, mew=1.5, alpha=0.5, label='obserwacje')
    # plt.plot(x, fit_fn0(x), 'k-', alpha=0.6)
    plt.plot(p_x, p_y, 'r--', alpha=1, label='linia regresji')
    plt.plot(p_x, lower, 'b--', alpha=0.4, label='przedział ufności (95%)')
    plt.plot(p_x, upper, 'b--', alpha=0.4)
    plt.ylabel('y', fontsize=20, labelpad=8)
    plt.xlabel('x', fontsize=20, labelpad=8)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.grid(True, alpha=0.5)
    legend = plt.legend(fontsize=14, frameon=True)
    legend.get_frame().set_edgecolor('#555555')
    legend.get_frame().set_facecolor('#dddddd')
    legend.get_frame().set_linewidth(1.0)
    plt.tight_layout()
    # legend.get_frame().set_facecolor('#bbbbbb')


# density = 50
# np.random.seed(38)
# x = np.random.random_sample(density)*100
# y = -np.random.random_sample(density)*50+100


# density = 100
# np.random.seed(10)
# x = np.random.random_sample(density)*100-50
# y = np.random.random_sample(density)*np.random.random()*30+x*x*0.03


density = 50
np.random.seed(28)
x = np.random.random_sample(density)*10-5
y = np.random.random_sample(density)*np.random.random()*4-x*0.4


fig = plt.figure(figsize=(9, 9), dpi=100)
title = 'Współczynnik korelacji, r = {0:.4f}'.format(sci.pearsonr(x, y)[0])
perform_t1c(x, y, plot_title=title)
print sci.pearsonr(x, y)[0]
# plt.xlim(-1, 1)
# plt.ylim(-0.5, 1)
plt.axis('equal')
plt.show()
