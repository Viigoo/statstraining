# coding=utf-8
# import math, time, sys, random, datetime, numpy
import random, datetime, numpy, time

def print_styles(input_string, style=1, foreg=30, backg=48):
    style_new = style
    foreg_new = foreg
    backg_new = backg
    if style > 8 or style < 0:
        style_new = 0
    if foreg > 38 or foreg < 30:
        foreg_new = 30
    if backg > 48 or backg < 40:
        backg_new = 48
    color_format = ';'.join([str(style_new), str(foreg_new), str(backg_new)])
    s = '\x1b[%sm%s\x1b[0m' % (color_format, input_string)
    print(s),

def eval_reward(winAmount):
    if winAmount == 0:
        return 120
    if winAmount == 1:
        return 130
    if winAmount == 2:
        return 140
    if winAmount == 3:
        return 150
    if winAmount == 4:
        return 160
    if winAmount == 5:
        return 180
    if winAmount == 6:
        return 210
    if winAmount == 7:
        return 270
    if winAmount == 8:
        return 320
    if winAmount == 9:
        return 350
    if winAmount == 10:
        return 380
    if winAmount == 11:
        return 410
    if winAmount == 12:
        return 480
    return -1

MAX_GAMES = 15
generation = 0
i = 0
loses = 0
rand = 0
winThreshold = 0
tempThresh = 0
wins = 0
rewards_list = []
wins_list = []

while tempThresh <= 100:
    generation = 0
    rewards_list[:] = []
    wins_list[:] = []
    rewards_list.append(eval_reward(wins))
    while generation < 10000:
        # print 'Run:\t',
        # print_styles('Run:', 2, 36)
        # print '%d:%.2d:%.2d\t' % (datetime.datetime.now().time().hour,
        #                           datetime.datetime.now().time().minute,
        #                           datetime.datetime.now().time().second),
        loses = 0
        i = 0
        wins = 0
        while i < MAX_GAMES:
            rand = random.randint(0, 100)
            # winThreshold = 50 - i + loses # every game % to win is lower like 70% 67% 64% ...
            winThreshold = tempThresh - (3*i + 3*loses)*0
            if rand < (winThreshold):
                # print 'W',
                wins += 1
                # print_styles('W', 2, 32)
            else:
                loses += 1
                # print 'L',
                # print_styles('L', 2, 31)
            # print '%d < %d' % (rand, winThreshold)
            # time.sleep(0.25)
            if loses > 2 or wins == 12:
                break
            i += 1
        wins_list.append(wins)
        rewards_list.append(eval_reward(wins))
        # print '%d' % rewards[generation],
        # print '%.1f, %.2f' % (numpy.mean(rewards_list), numpy.mean(wins_list))
        # time.sleep(0.25)

        generation += 1
    print_styles('First game win%% = ', 2, 36)
    print '%3.f%%,\tAVG reward = %.1f,   AVG wins = %.2f\tSTDDEV = %.2f' % (tempThresh, numpy.mean(rewards_list), numpy.mean(wins_list), numpy.std(wins_list))
    tempThresh += 5
