# -*- coding: utf-8 -*-
import time, math

TIME_INTERVAL = 0.32123
sum = 0
clocktime = 0
previous_time = 0
diff = 0
for i in range(10):
    # print '%g\b' % i,
    clocktime = time.clock()
    diff = clocktime - previous_time
    previous_time = clocktime
    sum += diff
    print i, clocktime, diff, sum
    time.sleep(TIME_INTERVAL)
