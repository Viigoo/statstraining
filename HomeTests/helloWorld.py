# -*- coding: utf-8 -*-
"""
Created on Thu Aug 03 00:51:16 2017

@author: northshire
"""
from math import sqrt

print 'hello world! test'
items = range(1, 1000)


def prime(item):
    if item < 2:
        return False
    for i in range(2, int(sqrt(item))):
        if item % i == 0:
            return False
    return True


def is_lower_than10(item):
    if item <= 10:
        return True
    return False

primes = list(filter(prime, items))
lower10 = list(filter(is_lower_than10, items))

index = 1
for j in primes:
    print '{i:>3.0f}'.format(i = j),
    if index % 50 == 0:
        print ''
    index += 1

print ''
for j in lower10:
    print '{i:>3.0f}'.format(i = j),
    if index % 50 == 0:
        print ''
    index += 1