# coding=utf-8
import numpy as np

# arc length of x^2
x = np.linspace(0, 1, 99999)
y = x**80

arc_length = 0
last_x = x[0]
last_y = y[0]
for i in range(2, len(x)):
    current_x = x[i]
    current_y = y[i]
    arc_length += ((last_x - current_x)**2 + (last_y - current_y)**2)**0.5
    last_x = current_x
    last_y = current_y

print arc_length