class Color(int):
    WHITE = 30
    RED = 31
    GREEN = 32
    BROWN = 33
    LIGHT_BLUE = 34
    MAGENTA = 35
    CYAN = 36
    GRAY = 37


def print_format_table():
    """
    prints table of formatted text format options
    """
    for style in range(8):
        for fg in range(30, 38):
            s1 = ''
            for bg in range(40, 48):
                string_format = ';'.join([str(style), str(fg), str(bg)])
                s1 += '\x1b[%sm %s \x1b[0m' % (string_format, string_format)
            print(s1)
        print('\n')


def print_styles(input_string, style=1, foreg=30, backg=48):
    style_new = style
    foreg_new = foreg
    backg_new = backg
    if style > 8 or style < 0:
        style_new = 0
    if foreg > 38 or foreg < 30:
        foreg_new = 30
    if backg > 48 or backg < 40:
        backg_new = 48
    color_format = ';'.join([str(style_new), str(foreg_new), str(backg_new)])
    s = '\x1b[%sm%s\x1b[0m' % (color_format, input_string)
    print(s),

# for i in range(39, 48):
#     for j in range(30, 38):
#         print_styles('test', 1, j, i)
#     print ''
#
# print_styles('Life is beautiful!', 4, Color.RED, 0 + 10)
#
# print ''

# i = 1000 #nie ma powtarzalnosci?
# while i > 0:
#     print 't',
#     print '\b\b\b%g', i,
#     i -= 1
