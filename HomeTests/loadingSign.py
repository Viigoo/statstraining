# coding=utf-8
import itertools
import threading
import time
import sys

print 'Processing a request...'
isDone = False
if isDone:
    print 'lol'


def animate():
    for c in itertools.cycle(['.', '..', '...', '...']):
        if isDone:
            break
        sys.stdout.write('\rloading ' + c)
        sys.stdout.flush()
        time.sleep(0.25)
    sys.stdout.write('\rDone!')

t = threading.Thread(target=animate)
t.start()

time.sleep(4)
isDone = True
