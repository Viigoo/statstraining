# coding=utf-8
import random, time
from colorTextTest import print_styles

INITIAL_AMOUNT_OF_CARDS = 3
deck = range(10, 40)
hand = []

print_styles('Hearthstone probabilities:', 4, 36, 48)
print ''
print_styles(str(deck), 1, 37, 48)


def clear_all():
    global hand
    global deck
    hand = []
    deck = range(10, 40)
    random.shuffle(deck)
    hand = deck[:INITIAL_AMOUNT_OF_CARDS]
    deck = deck[INITIAL_AMOUNT_OF_CARDS:]


def get_draw_density_of_cards(card, trials):
    global deck
    global hand
    density_array = []
    for i in range(len(deck + hand)):
        density_array.append(0)
    for i in range(trials):
        clear_all()
        density_array[(hand + deck).index(card)] += 1
    return density_array


def print_highlighted_cards(cards, investigated_array):
    is_anything_found = False
    print_styles(cards, 1, 35, 48)
    print ' -> ',
    print_styles('[', 1, 36, 48)
    for item in investigated_array:
        if item in cards:
            print_styles(item, 7, 32, 48)  # found - green
            is_anything_found = True
        else:
            print_styles(item, 1, 37, 48)  # not found
    print_styles(']', 1, 36, 48)
    if not is_anything_found:
        print_styles(' -> not found', 4, 31, 48)
    else:
        print_styles(' -> match    ', 2, 32, 48)
    return is_anything_found


def print_highlighted_if_in_first_n_turns(cards, n):
    print cards,
    print 'before turn:',
    print n,
    print ' -> ',
    is_on_time = True
    for item in cards:
        if item in deck:
            if deck.index(item) >= n:
                is_on_time = False

    print '[',
    if is_on_time:
        for item in deck:
            print_styles(item, 1, 32, 48)
    else:
        for item in deck:
            print_styles(item, 1, 31, 48)
    print ']',
    return is_on_time


def draw_card():
    global deck
    global hand
    hand.append(deck.pop(0))


def mulligan(cards_to_replace):
    global deck
    global hand
    to_remove = []
    for item in hand:
        if item in cards_to_replace:
            to_remove.append(item)
    for item in to_remove:
        hand.remove(item)
    for i in range(len(to_remove)):
        draw_card()
        random_index = random.randint(len(to_remove) + i - 1, len(deck))
        deck.insert(random_index, to_remove.pop(0))

print '\nTest:'
print_highlighted_if_in_first_n_turns([10, 13], 4)
print ''
print_highlighted_if_in_first_n_turns([10, 20], 4)
print ''
print '%s' % '-'*125, '\n'
clear_all()
print hand,
print deck

card = 15  # 15 - let's say patches
TRIALS = 6
stats_before_mulligan = []
stats_after_mulligan = []
for x in range(len(deck + hand)):
    stats_before_mulligan.append(0)
    stats_after_mulligan.append(0)
i = 0
while i < TRIALS:
    clear_all()
    print_styles(time.strftime("%Y-%m-%d %a %H:%M:%S"), 1, 33, 48)
    print_highlighted_cards([card], hand)
    print_highlighted_cards([card], deck)
    mulligan([card])
    print_highlighted_cards([card], hand)
    print_highlighted_cards([card], deck)
    time.sleep(0.25)
    print ''
    i += 1
print stats_before_mulligan
