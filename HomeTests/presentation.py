# coding=utf-8
import pandas as pd
pd.set_option('display.max_rows', 10)
pd.set_option('display.max_columns', 40)
pd.set_option('display.width', 300)

file_name = 'Titanic.csv'
data = pd.read_csv(file_name)
print data.head(8)


print '...'
print '[1309 rows x 10 columns]'


# Unique values of variables
uniq_home = len(data['Home'].unique())
uniq_name = len(data['Name'].unique())

# Find indexes of observations where value has already appeared
duplicates = data['Name'].duplicated()
duplicates.loc[duplicates == True]

# Show missing values
print data.isnull().sum()

# Remove variables
data.drop('Home', 1, inplace=True)
data.drop('Name', 1, inplace=True)

# Replace Age and Fare with median, port with most often occuring
data["Age"].fillna(data["Age"].median(skipna=True), inplace=True)
data['Fare'].fillna(data['Fare'].median(skipna=True), inplace=True)
data['Port'].fillna(data['Port'].value_counts().idxmax(), inplace=True)


















































































































































a = 0

cat_vars = ['PC', 'Sex', 'Port']
for var in cat_vars:
    cat_list = pd.get_dummies(data[var], prefix=var)
    data = data.join(cat_list)

# "-1" coding
data['PC1'] = data.apply(
    lambda row: int(-1) if row['PC_3'] == 1 else int(row['PC_1']),
    axis=1
)
data['PC2'] = data.apply(
    lambda row: int(-1) if row['PC_3'] == 1 else int(row['PC_3']),
    axis=1
)
data['Female'] = data.apply(
    lambda row: int(1) if row['Sex_female'] == 1 else int(-1),
    axis=1
)
data['C'] = data.apply(
    lambda row: int(-1) if row['Port_S'] == 1 else int(row['Port_C']),
    axis=1
)
data['Q'] = data.apply(
    lambda row: int(-1) if row['Port_S'] == 1 else int(row['Port_Q']),
    axis=1
)
print data.head()





















































































































































import statsmodels.api as sm

Y = ['Survived']
X = [i for i in data if i not in Y]

model = sm.Logit(data[Y], data[X])
result = model.fit()
print result.summary()




































































































































def forward_selected(predictors, response, alpha=0.05, locked=[], data=[]):
    winners = list(locked)
    scores_with_candidates = []
    remaining = list(predictors)
    r = list(response)
    current_score = 0

    for lock in locked:
        if lock in remaining:
            remaining.remove(lock)

    while remaining:
        for candidate in remaining:
            temp_model = sm.Logit(data[r], data[winners + [candidate]])
            temp_result = temp_model.fit(disp=False)
            score = temp_result.prsquared
            pval = pd.DataFrame(temp_result.pvalues).at[candidate, 0]
            scores_with_candidates.append((score, candidate, pval))
        scores_with_candidates.sort()
        best_new_score, best_candidate, best_candidate_pvalue = scores_with_candidates.pop()
        if best_new_score > current_score and best_candidate_pvalue <= alpha:
            winners.append(best_candidate)
            remaining.remove(best_candidate)
            current_score = best_new_score
        else:
            break
    return winners

































import numpy as np
data['is_train'] = np.random.uniform(0, 1, len(data)) <= 0.7
train, test = data[data['is_train'] == True], data[data['is_train'] == False]
predictors=['Passenger_Class', 'Sex_female', 'Age', 'Siblings_and_Spouses',
            'Parents_and_Children', 'Fare', 'Port_C', 'Port_Q', 'Port_S']
target = ['Survived']


from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz

dt = DecisionTreeClassifier()
dt = dt.fit(data[predictors], data[target])
with open('DataFiles/tree.dot', 'w') as dotfile:
    export_graphviz(dt, out_file=dotfile, feature_names=predictors)



# "-1" coding
data['PassengerClass1'] = data.apply(
    lambda row: int(-1) if row['Passenger_Class_3'] == 1 else int(row['Passenger_Class_1']),
    axis=1
)
data['PassengerClass2'] = data.apply(
    lambda row: int(-1) if row['Passenger_Class_3'] == 1 else int(row['Passenger_Class_3']),
    axis=1
)
data['SexFemale'] = data.apply(
    lambda row: int(1) if row['Sex_female'] == 1 else int(-1),
    axis=1
)
data['PortC'] = data.apply(
    lambda row: int(-1) if row['Port_S'] == 1 else int(row['Port_C']),
    axis=1
)
data['PortQ'] = data.apply(
    lambda row: int(-1) if row['Port_S'] == 1 else int(row['Port_Q']),
    axis=1
)
# Manually add intercept
data['intercept'] = 1.0






